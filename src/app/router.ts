import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AuthGuard, AdminGuard } from '@modules/global/guards/index.guards';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', loadChildren: 'app/modules/home/home.module#HomeModule' },
    { path: 'dashboard', canActivate: [AuthGuard], loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule' },
    { path: 'admin', canActivate: [AdminGuard], loadChildren: 'app/modules/admin/admin.module#AdminModule' }
];

export const AppRouter: ModuleWithProviders = RouterModule.forRoot(appRoutes);
