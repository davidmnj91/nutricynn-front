import { AbstractModel } from '@models/index.models';

export interface ArticleCategory extends AbstractModel {
    name: string;
    fatherCategory: ArticleCategory;
}

export class ArticleCategory implements ArticleCategory {
    constructor(id?: string, name?: string, fatherCategory?: ArticleCategory) {
        this.id = id;
        this.name = name;
        this.fatherCategory = fatherCategory;
    }
}
