import { NutrientCategory, AbstractModel } from './index.models';

export interface Nutrient extends AbstractModel {
    name: string;
    iconName: string;
    category: NutrientCategory;
    energy: number;
    carbs: number;
    fats: number;
    proteins: number;
}

export class Nutrient implements Nutrient {
    constructor(id?: string, name?: string, iconName?: string, category?: NutrientCategory,
        energy?: number, carbs?: number, fats?: number, proteins?: number) {
        this.id = id;
        this.name = name;
        this.iconName = iconName;
        this.category = category;
        this.energy = energy;
        this.carbs = carbs;
        this.fats = fats;
        this.proteins = proteins;
    }
}
