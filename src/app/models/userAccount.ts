import { AbstractModel, UserRol } from '@models/index.models';
import { UserPolls } from '@models/polls/index.polls';

export interface UserAccount extends AbstractModel {
    username: string;
    email: string;
    password: string;
    roles: Set<UserRol>;
    expired: boolean;
    locked: boolean;
    credentialsExpired: boolean;
    enabled: boolean;
    name: string;
    lastName: string;
    photoPath: string;
    birthday: Date;
    phone: string;
    address: string;
    weight: number;
    height: number;
    userPolls: UserPolls;
}

export class UserAccount implements UserAccount {
    constructor(id?: string, username?: string, email?: string, password?: string, roles?: Set<UserRol>, expired?: boolean,
        locked?: boolean, credentialsExpired?: boolean, enabled?: boolean, name?: string, lastName?: string, photoPath?: string,
        birthday?: Date, phone?: string, address?: string, weight?: number, height?: number, userPolls?: UserPolls) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.expired = expired;
        this.locked = locked;
        this.credentialsExpired = credentialsExpired;
        this.enabled = enabled;
        this.name = name;
        this.lastName = lastName;
        this.photoPath = photoPath;
        this.birthday = birthday;
        this.phone = phone;
        this.address = address;
        this.weight = weight;
        this.height = height;
        this.userPolls = userPolls;
    }
}
