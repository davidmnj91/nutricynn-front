import { DietMeal, WeekDaysEnum, AbstractModel } from '@models/index.models';

export interface DietDay extends AbstractModel {
    day: WeekDaysEnum;
    meals: DietMeal[];
}

export class DietDay implements DietDay {
    constructor(id?: string, day?: WeekDaysEnum, meals?: DietMeal[]) {
        this.id = id;
        this.day = day;
        this.meals = meals;
    }

    findMeal(meal: DietMeal): number {
        return this.meals.indexOf(meal);
    }
}
