import { MealName, DietFood, AbstractModel } from '@models/index.models';

export interface DietMeal extends AbstractModel {
    mealName: MealName;
    time: string;
    foods: DietFood[];
}

export class DietMeal implements DietMeal {
    constructor(id?: string, mealName?: MealName, time?: string, foods?: DietFood[]) {
        this.id = id;
        this.mealName = name;
        this.time = time;
        this.foods = foods;
    }

    findFood(food: DietFood): number {
        return this.foods.indexOf(food);
    }
}
