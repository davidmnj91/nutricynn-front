export interface DietProgress {
    date: Date;
    weight: number;
    fatPercentage: number;
    waterPercentage: number;
    musclePercentage: number;
}

export class DietProgress implements DietProgress {
    constructor(date?: Date, weight?: number, fatPercentage?: number, waterPercentage?: number, musclePercentage?: number) {
        this.date = date;
        this.weight = weight;
        this.fatPercentage = fatPercentage;
        this.waterPercentage = waterPercentage;
        this.musclePercentage = musclePercentage;
    }
}
