import { AbstractModel } from '@models/index.models';

export interface EventCategory extends AbstractModel {
    name: string;
    fatherCategory: EventCategory;
}

export class EventCategory implements EventCategory {
    constructor(id?: string, name?: string, fatherCategory?: EventCategory) {
        this.id = id;
        this.name = name;
        this.fatherCategory = fatherCategory;
    }
}
