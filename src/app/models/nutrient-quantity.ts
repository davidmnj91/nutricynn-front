import { AbstractModel, Nutrient, NutrientUnit } from '@models/index.models';


export interface NutrientQuantity extends AbstractModel {
    nutrient: Nutrient;
    quantity: number;
    unit: NutrientUnit;
}

export class NutrientQuantity implements NutrientQuantity {
    constructor(id?: string, nutrient?: Nutrient, quantity?: number, unit?: NutrientUnit) {
        this.id = id;
        this.nutrient = nutrient;
        this.quantity = quantity;
        this.unit = unit;
    }

    getPath(): string {
        return 'nutrientQuantity';
    }
}
