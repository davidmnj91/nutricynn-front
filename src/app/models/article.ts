import { AbstractModel, ArticleCategory } from '@models/index.models';

export interface Article extends AbstractModel {
    name: string;
    category: ArticleCategory;
    content: string;
}

export class Article implements Article {
    constructor(id?: string, name?: string, category?: ArticleCategory, content?: string) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.content = content;
    }
}
