import { AbstractModel } from '@models/index.models';

export interface MealName extends AbstractModel {
    name: string;
}

export class MealName implements MealName {
    constructor(id?: string, name?: string) {
        this.id = id;
        this.name = name;
    }
}
