import { AbstractModel } from '@models/index.models';

export interface NutrientUnit extends AbstractModel {
    name: string;
    shortName: string;
    conversion: number;
    baseUnit: NutrientUnit;
    icon: string;
}

export class NutrientUnit implements NutrientUnit {
    constructor(id?: string, name?: string, shortName?: string, conversion?: number,
        baseUnit?: NutrientUnit, icon?: string) {
        this.id = id;
        this.name = name;
        this.shortName = shortName;
        this.conversion = conversion;
        this.baseUnit = baseUnit;
        this.icon = icon;
    }
}
