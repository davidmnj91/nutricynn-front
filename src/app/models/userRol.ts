import { AbstractModel } from '@models/index.models';

export interface UserRol extends AbstractModel {
    name: string;
}

export class UserRol implements UserRol {
    constructor(id?: string, name?: string) {
        this.id = id;
        this.name = name;
    }
}
