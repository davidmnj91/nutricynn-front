export enum FoodAmountEnum {
    ONE,
    OME_TO_THREE,
    THREE_TO_FIVE,
    MORE_FIVE
}

export const FoodAmount = new Map<number, string>([
    [FoodAmountEnum.ONE, '1 Comida'],
    [FoodAmountEnum.OME_TO_THREE, '1-3 Comidas'],
    [FoodAmountEnum.THREE_TO_FIVE, '3-5 Comidas'],
    [FoodAmountEnum.MORE_FIVE, '+5 Comidas'],
]);
