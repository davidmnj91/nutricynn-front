export enum SleepHoursEnum {
    LESS_FIVE,
    FIVE_TO_SEVEN,
    SEVEN,
    MORE_SEVEN
}

export const SleepHours = new Map<number, string>([
	[SleepHoursEnum.LESS_FIVE, '-5 Horas'],
    [SleepHoursEnum.FIVE_TO_SEVEN, '5-7 Horas'],
    [SleepHoursEnum.SEVEN, '7 Horas'],
    [SleepHoursEnum.MORE_SEVEN, '+7 Horas']
]);