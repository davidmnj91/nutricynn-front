export enum BiologicalStatusEnum {
    GESTATION,
    LACTATION,
    MENOPAUSE,
    NONE
}

export const BiologicalStatus = new Map<number, string>([
    [BiologicalStatusEnum.GESTATION, 'Gestación'],
    [BiologicalStatusEnum.LACTATION, 'Lactancia'],
    [BiologicalStatusEnum.MENOPAUSE, 'Menopausia'],
    [BiologicalStatusEnum.NONE, 'Ninguno']
]);
