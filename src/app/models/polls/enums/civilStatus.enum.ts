export enum CivilStatusEnum {
    MARRIED,
    SINGLE,
    WIDOWER
}

export const CivilStatus = new Map<number, string>([
	[CivilStatusEnum.MARRIED, 'Casado'],
    [CivilStatusEnum.SINGLE, 'Soltero'],
    [CivilStatusEnum.WIDOWER, 'Viudo']
]);