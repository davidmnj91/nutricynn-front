export enum WaterAmountEnum {
    NONE,
    HALF,
    ONE,
    ONE_HALF,
    TWO,
    MORE_TWO
}

export const WaterAmount = new Map<number, string>([
    [WaterAmountEnum.NONE, 'Nada'],
    [WaterAmountEnum.HALF, '0.5 Litros'],
    [WaterAmountEnum.ONE, '1 Litro'],
    [WaterAmountEnum.ONE_HALF, '1.5 Litros'],
    [WaterAmountEnum.TWO, '2 Litros'],
    [WaterAmountEnum.MORE_TWO, '+2 Litros']
]);
