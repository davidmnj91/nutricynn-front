export enum CheatTimeEnum {
    NONE,
    MORNING,
    EVENING,
    ALWAYS
}

export const CheatTime = new Map<number, string>([
	[CheatTimeEnum.NONE, 'Nunca'],
    [CheatTimeEnum.MORNING, 'Mañana'],
    [CheatTimeEnum.EVENING, 'Tarde'],
    [CheatTimeEnum.ALWAYS, 'Siempre']
]);