export enum FoodTypeEnum {
    NONE,
    LIGHT,
    HEAVY,
    BOTH
}

export const FoodType = new Map<number, string>([
    [FoodTypeEnum.NONE, 'Nunca'],
    [FoodTypeEnum.LIGHT, 'Light'],
    [FoodTypeEnum.HEAVY, 'Comida mas densa'],
    [FoodTypeEnum.BOTH, 'Ambas']
]);
