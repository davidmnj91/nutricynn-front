export enum FitnessReasonEnum {
    EAT_HEALTHY,
    HEALTH,
    AESTHETIC,
    FEEL_GOOD
}

export const FitnessReason = new Map<number, string>([
    [FitnessReasonEnum.EAT_HEALTHY, 'Comer Saludable'],
    [FitnessReasonEnum.HEALTH, 'Salud'],
    [FitnessReasonEnum.AESTHETIC, 'Estetica'],
    [FitnessReasonEnum.FEEL_GOOD, 'Sentirme Bien']
]);
