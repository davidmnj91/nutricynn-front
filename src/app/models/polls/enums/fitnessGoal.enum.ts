export enum FitnessGoalEnum {
    SLIMMING,
    MAINTENANCE,
    WEIGHT_GAIN,
    VOLUME,
    SHREDDING
}

export const FitnessGoal = new Map<number, string>([
	[FitnessGoalEnum.SLIMMING, 'Adelgazamiento'],
    [FitnessGoalEnum.MAINTENANCE, 'Mantenimineto'],
    [FitnessGoalEnum.WEIGHT_GAIN, 'Aumento de peso'],
    [FitnessGoalEnum.VOLUME, 'Volumen'],
    [FitnessGoalEnum.SHREDDING, 'Definicion']
]);