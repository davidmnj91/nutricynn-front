export enum UnitAmountEnum {
    NONE,
    ONE,
    OME_TO_THREE,
    THREE_TO_FIVE,
    MORE_FIVE
}

export const UnitAmount = new Map<number, string>([
	[UnitAmountEnum.NONE, 'Ninguno'],
    [UnitAmountEnum.ONE, '1'],
    [UnitAmountEnum.OME_TO_THREE, '1-3'],
    [UnitAmountEnum.THREE_TO_FIVE, '3-5'],
	[UnitAmountEnum.MORE_FIVE, '+5'],
]);