export enum ChefAmountEnum {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    MORE_FIVE
}

export const ChefAmount = new Map<number, string>([
    [ChefAmountEnum.ONE, '1 Persona'],
    [ChefAmountEnum.TWO, '2 Personas'],
    [ChefAmountEnum.THREE, '3 Personas'],
    [ChefAmountEnum.FOUR, '4 Personas'],
    [ChefAmountEnum.FIVE, '5 Personas'],
    [ChefAmountEnum.MORE_FIVE, '+5 Personas']
]);