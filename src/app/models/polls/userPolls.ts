import { PollBasicData } from '@models/polls/PollBasicData';
import { PollWeekEndDiet } from '@models/polls/PollWeekEndDiet';
import { PollWeekDiet } from '@models/polls/PollWeekDiet';
import { PollClinicData } from '@models/polls/PollClinicData';

export interface UserPolls {
    pollBasicData: PollBasicData;
    pollClinicData: PollClinicData;
    pollWeekEndDiet: PollWeekEndDiet;
    pollWeekDiet: PollWeekDiet;
}

export class UserPolls implements UserPolls {
    constructor(pollBasicData?: PollBasicData, pollClinicData?: PollClinicData, pollWeekEndDiet?: PollWeekEndDiet,
        pollWeekDiet?: PollWeekDiet) {
        this.pollBasicData = pollBasicData;
        this.pollClinicData = pollClinicData;
        this.pollWeekDiet = pollWeekDiet;
        this.pollWeekEndDiet = pollWeekEndDiet;
    }
}
