import { FoodTypeEnum, FoodAmountEnum, WaterAmountEnum, CheatTimeEnum } from '@models/polls/enums/index.enums';
import { AbstractPoll } from '@models/polls/index.polls';

export interface PollWeekDiet extends AbstractPoll {
  workingDayBreakfast: FoodAmountEnum;
  workingDayMidDay: boolean;
  workingDayMidDayDescription: FoodAmountEnum;
  workingDayLunch: FoodAmountEnum;
  workingDaySnack: boolean;
  workingDaySnackDescription: FoodAmountEnum;
  workingDayDinner: FoodAmountEnum;
  workingDayBed: boolean;
  workingDayBedDescription: FoodTypeEnum;
  workingDayWater: WaterAmountEnum;
  workingDayCheatFood: CheatTimeEnum;
  workingDayTonics: FoodAmountEnum;
  workingDayAlcohol: FoodAmountEnum;
  workingDaySmoke: boolean;
}

export class PollWeekDiet implements PollWeekDiet {
  constructor(workingDayBreakfast?: FoodAmountEnum, workingDayMidDay?: boolean, workingDayMidDayDescription?: FoodAmountEnum,
    workingDayLunch?: FoodAmountEnum, workingDaySnack?: boolean, workingDaySnackDescription?: FoodAmountEnum,
    workingDayDinner?: FoodAmountEnum, workingDayBed?: boolean, workingDayBedDescription?: FoodTypeEnum,
    workingDayWater?: WaterAmountEnum, workingDayCheatFood?: CheatTimeEnum, workingDayTonics?: FoodAmountEnum,
    workingDayAlcohol?: FoodAmountEnum, workingDaySmoke?: boolean) {
    this.workingDayBreakfast = workingDayBreakfast;
    this.workingDayMidDay = workingDayMidDay;
    this.workingDayMidDayDescription = workingDayMidDayDescription;
    this.workingDayLunch = workingDayLunch;
    this.workingDaySnack = workingDaySnack;
    this.workingDaySnackDescription = workingDaySnackDescription;
    this.workingDayDinner = workingDayDinner;
    this.workingDayBed = workingDayBed;
    this.workingDayBedDescription = workingDayBedDescription;
    this.workingDayWater = workingDayWater;
    this.workingDayCheatFood = workingDayCheatFood;
    this.workingDayTonics = workingDayTonics;
    this.workingDayAlcohol = workingDayAlcohol;
    this.workingDaySmoke = workingDaySmoke;
  }
}
