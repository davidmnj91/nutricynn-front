import { FoodTypeEnum, FoodAmountEnum, WaterAmountEnum, CheatTimeEnum } from '@models/polls/enums/index.enums';
import { AbstractPoll } from '@models/polls/index.polls';

export interface PollWeekEndDiet extends AbstractPoll {
  weekEndBreakfast: FoodAmountEnum;
  weekEndMidDay: boolean;
  weekEndMidDayDescription: FoodAmountEnum;
  weekEndLunch: FoodAmountEnum;
  weekEndSnack: boolean;
  weekEndSnackDescription: FoodAmountEnum;
  weekEndDinner: FoodAmountEnum;
  weekEndBed: boolean;
  weekEndBedDescription: FoodTypeEnum;
  weekEndWater: WaterAmountEnum;
  weekEndCheatFood: CheatTimeEnum;
  weekEndTonics: FoodAmountEnum;
  weekEndAlcohol: FoodAmountEnum;
  weekEndSmoke: boolean;
}

export class PollWeekEndDiet implements PollWeekEndDiet {
  constructor(weekEndBreakfast?: FoodAmountEnum, weekEndMidDay?: boolean, weekEndMidDayDescription?: FoodAmountEnum,
    weekEndLunch?: FoodAmountEnum, weekEndSnack?: boolean, weekEndSnackDescription?: FoodAmountEnum, weekEndDinner?: FoodAmountEnum,
    weekEndBed?: boolean, weekEndBedDescription?: FoodTypeEnum, weekEndWater?: WaterAmountEnum, weekEndCheatFood?: CheatTimeEnum,
    weekEndTonics?: FoodAmountEnum, weekEndAlcohol?: FoodAmountEnum, weekEndSmoke?: boolean) {
    this.weekEndBreakfast = weekEndBreakfast;
    this.weekEndMidDay = weekEndMidDay;
    this.weekEndMidDayDescription = weekEndMidDayDescription;
    this.weekEndLunch = weekEndLunch;
    this.weekEndSnack = weekEndSnack;
    this.weekEndSnackDescription = weekEndSnackDescription;
    this.weekEndDinner = weekEndDinner;
    this.weekEndBed = weekEndBed;
    this.weekEndBedDescription = weekEndBedDescription;
    this.weekEndWater = weekEndWater;
    this.weekEndCheatFood = weekEndCheatFood;
    this.weekEndTonics = weekEndTonics;
    this.weekEndAlcohol = weekEndAlcohol;
    this.weekEndSmoke = weekEndSmoke;
  }
}
