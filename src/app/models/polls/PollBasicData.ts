import { FitnessGoalEnum, FoodAmountEnum, CivilStatusEnum, ChefAmountEnum, SleepHoursEnum } from '@models/polls/enums/index.enums';
import { AbstractPoll } from '@models/polls/index.polls';

export interface PollBasicData extends AbstractPoll {
  fitnessGoal: FitnessGoalEnum;
  fitnessReason: string;
  physicalActivity: boolean;
  physicalActivityDescription: string;
  foodNumber: FoodAmountEnum;
  workInfo: boolean;
  workInfoDescription: string;
  civilStatus: CivilStatusEnum;
  familyInfo: boolean;
  homeInfo: boolean;
  cookingInfo: ChefAmountEnum;
  sleepWorkingInfo: SleepHoursEnum;
  sleepWeekendInfo: SleepHoursEnum;
}

export class PollBasicData implements PollBasicData {
  constructor(fitnessGoal?: FitnessGoalEnum, fitnessReason?: string, physicalActivity?: boolean,
    physicalActivityDescription?: string, foodNumber?: FoodAmountEnum, workInfo?: boolean, workInfoDescription?: string,
    civilStatus?: CivilStatusEnum, familyInfo?: boolean, homeInfo?: boolean, cookingInfo?: ChefAmountEnum,
    sleepWorkingInfo?: SleepHoursEnum, sleepWeekendInfo?: SleepHoursEnum) {
    this.fitnessGoal = fitnessGoal;
    this.fitnessReason = fitnessReason;
    this.physicalActivity = physicalActivity;
    this.physicalActivityDescription = physicalActivityDescription;
    this.foodNumber = foodNumber;
    this.workInfo = workInfo;
    this.workInfoDescription = workInfoDescription;
    this.civilStatus = civilStatus;
    this.familyInfo = familyInfo;
    this.homeInfo = homeInfo;
    this.cookingInfo = cookingInfo;
    this.sleepWorkingInfo = sleepWorkingInfo;
    this.sleepWeekendInfo = sleepWeekendInfo;
  }
}
