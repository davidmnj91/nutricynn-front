import { BiologicalStatusEnum } from '@models/polls/enums/index.enums';
import { AbstractPoll } from '@models/polls/index.polls';

export interface PollClinicData extends AbstractPoll {
  importantIllness: boolean;
  importantIllnessDescription: string;
  biologicalStatus: BiologicalStatusEnum;
  eatingDisorder: boolean;
  medicineRecord: boolean;
  medicineRecordDescription: string;
}

export class PollClinicData implements PollClinicData {
  constructor(importantIllness?: boolean, importantIllnessDescription?: string, biologicalStatus?: BiologicalStatusEnum,
    eatingDisorder?: boolean, medicineRecord?: boolean, medicineRecordDescription?: string) {
    this.importantIllness = importantIllness;
    this.importantIllnessDescription = importantIllnessDescription;
    this.biologicalStatus = biologicalStatus;
    this.eatingDisorder = eatingDisorder;
    this.medicineRecord = medicineRecord;
    this.medicineRecordDescription = medicineRecordDescription;
  }
}


