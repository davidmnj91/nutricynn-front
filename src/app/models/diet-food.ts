import { NutrientQuantity, AbstractModel } from '@models/index.models';

export interface DietFood extends AbstractModel {
    name: string;
    nutrients: NutrientQuantity[];
}

export class DietFood implements DietFood {
    constructor(id?: string, name?: string, nutrients?: NutrientQuantity[]) {
        this.id = id;
        this.name = name;
        this.nutrients = nutrients;
    }

    findNutrient(nutrient: NutrientQuantity): number {
        return this.nutrients.indexOf(nutrient);
    }
}
