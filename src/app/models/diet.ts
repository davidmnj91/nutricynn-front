import { WeekDaysEnum } from '@models/weekDays.enum';
import { AbstractModel } from '@models/abstract-model';
import { DietDay } from '@models/diet-day';
import { WeekDays } from '@models/index.models';


export interface Diet extends AbstractModel {
    name: string;
    description: string;
    dietDays: DietDay[];
    createDate: Date;
    updateDate: Date;
    baseDiet: boolean;
}

export class Diet implements Diet {
    constructor(id?: string, name?: string, description?: string, dietDays?: DietDay[],
        createDate?: Date, updateDate?: Date, baseDiet?: boolean) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dietDays = this.buildDays();
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.baseDiet = baseDiet;
    }

    findDay(dietDay: DietDay): number {
        return this.dietDays.indexOf(this.dietDays.filter(e => e === dietDay)[0]);
    }

    private buildDays(): DietDay[] {
        const days: DietDay[] = new Array<DietDay>();

        days.push(new DietDay(null, WeekDaysEnum.MONDAY));
        days.push(new DietDay(null, WeekDaysEnum.TUESDAY));
        days.push(new DietDay(null, WeekDaysEnum.WEDNESDAY));
        days.push(new DietDay(null, WeekDaysEnum.THURSDAY));
        days.push(new DietDay(null, WeekDaysEnum.FRIDAY));
        days.push(new DietDay(null, WeekDaysEnum.SATURDAY));
        days.push(new DietDay(null, WeekDaysEnum.SUNDAY));

        return days;
    }
}
