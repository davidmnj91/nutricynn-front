export enum WeekDaysEnum {
    MONDAY = <any>'LUNES',
    TUESDAY = <any>'MARTES',
    WEDNESDAY = <any>'MIERCOLES',
    THURSDAY = <any>'JUEVES',
    FRIDAY = <any>'VIERNES',
    SATURDAY = <any>'SABADO',
    SUNDAY = <any>'DOMINGO',
}

export const WeekDays = new Map<number, string>([
    [WeekDaysEnum.MONDAY, 'Lunes'],
    [WeekDaysEnum.TUESDAY, 'Martes'],
    [WeekDaysEnum.WEDNESDAY, 'Miercoles'],
    [WeekDaysEnum.THURSDAY, 'Jueves'],
    [WeekDaysEnum.FRIDAY, 'Viernes'],
    [WeekDaysEnum.SATURDAY, 'Sabado'],
    [WeekDaysEnum.SUNDAY, 'Domingo']
]);
