import { AbstractModel, Diet, DietProgress, UserAccount } from '@models/index.models';

export interface UserDiet extends AbstractModel {
    userAcount: UserAccount;
    startDate: Date;
    endDate: Date;
    diet: Diet;
    dietProgress: DietProgress[];
}

export class UserDiet implements UserDiet {
    constructor(id?: string, userAcount?: UserAccount, startDate?: Date, endDate?: Date, diet?: Diet, dietProgress?: DietProgress[]) {
        this.id = id;
        this.userAcount = userAcount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.diet = diet;
        this.dietProgress = dietProgress;
    }
}
