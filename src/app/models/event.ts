import { EventCategory, AbstractModel } from './index.models';

export interface Event extends AbstractModel {
    name: string;
    content: string;
    category: EventCategory;
    dateTime: DateTimeFormat;
    place: string;
}

export class Event implements Event {
    constructor(id?: string, name?: string, content?: string, category?: EventCategory, dateTime?: DateTimeFormat, place?: string) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.dateTime = dateTime;
        this.place = place;
    }
}
