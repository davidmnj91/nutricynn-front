import { AbstractModel } from '@models/index.models';

export interface NutrientCategory extends AbstractModel {
    name: string;
    fatherCategory: NutrientCategory;
}

export class NutrientCategory implements NutrientCategory {
    constructor(id?: string, name?: string, fatherCategory?: NutrientCategory) {
        this.id = id;
        this.name = name;
        this.fatherCategory = fatherCategory;
    }
}
