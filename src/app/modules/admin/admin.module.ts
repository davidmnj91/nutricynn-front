import { ModuleWithProviders, NgModule } from '@angular/core';
import {
    AdminArticleCategoriesEditorComponent,
    AdminArticleCategoriesPanelComponent,
    AdminArticlesEditorComponent,
    AdminArticlesPanelComponent,
    AdminDietEditorComponent,
    AdminDietFoodEditorComponent,
    AdminDietMealEditorComponent,
    AdminDietNutrientEditorComponent,
    AdminDietPanelComponent,
    AdminEventCategoriesEditorComponent,
    AdminEventCategoriesPanelComponent,
    AdminEventEditorComponent,
    AdminEventsPanelComponent,
    AdminHeaderComponent,
    AdminMainComponent,
    AdminMealNameEditorComponent,
    AdminMealNamePanelComponent,
    AdminNutrientCategoriesEditorComponent,
    AdminNutrientCategoriesPanelComponent,
    AdminNutrientsEditorComponent,
    AdminNutrientsPanelComponent,
    AdminNutryIndexComponent,
    AdminUnitsEditorComponent,
    AdminUnitsPanelComponent,
    AdminUserEditorComponent,
    AdminUserPanelComponent,
    SidebarAdminMenuComponent,
    UserDietEditorComponent,
    UserDietHistoryTabComponent,
    UserPollsTabComponent,
    UserProfileEditorComponent,
    UserProfileTabComponent,
} from '@modules/admin/components/index.components';
import { GlobalModule } from '@modules/global/global.module';
import { UiModule } from '@modules/ui/ui.module';
import { DataTableModule, EditorModule, TreeTableModule } from 'primeng/primeng';

import { AdminRouter } from './router';

@NgModule({
    declarations: [
        AdminMainComponent, AdminHeaderComponent, SidebarAdminMenuComponent, AdminUserPanelComponent, AdminNutryIndexComponent,
        AdminDietPanelComponent, AdminUnitsPanelComponent, AdminNutrientsPanelComponent, AdminNutrientCategoriesPanelComponent,
        AdminMealNamePanelComponent,
        AdminDietEditorComponent, AdminNutrientCategoriesEditorComponent, AdminNutrientsEditorComponent, AdminUnitsEditorComponent,
        AdminMealNameEditorComponent, AdminDietFoodEditorComponent, AdminDietMealEditorComponent, AdminDietNutrientEditorComponent,
        AdminArticlesPanelComponent, AdminArticlesEditorComponent, AdminArticleCategoriesPanelComponent,
        AdminArticleCategoriesEditorComponent, AdminEventsPanelComponent, AdminEventCategoriesPanelComponent, AdminEventEditorComponent,
        AdminEventCategoriesEditorComponent, AdminUserEditorComponent, UserDietEditorComponent, UserProfileTabComponent,
        UserProfileEditorComponent, UserDietHistoryTabComponent, UserPollsTabComponent
    ],
    imports: [
        GlobalModule,
        UiModule,
        AdminRouter,
        DataTableModule,
        TreeTableModule,
        EditorModule
    ],
    entryComponents: [
        AdminDietEditorComponent, AdminMealNameEditorComponent, AdminNutrientsEditorComponent, AdminNutrientCategoriesEditorComponent,
        AdminUnitsEditorComponent, AdminArticlesEditorComponent, AdminArticleCategoriesEditorComponent, AdminEventEditorComponent,
        AdminEventCategoriesEditorComponent, AdminUserEditorComponent, UserProfileEditorComponent,  UserDietEditorComponent
    ]
})
export class AdminModule {
    static forRoot(): ModuleWithProviders {
        return { ngModule: AdminModule };
    }
}
