import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNutryIndexComponent } from './admin-nutry-index.component';

describe('AdminNutryIndexComponent', () => {
  let component: AdminNutryIndexComponent;
  let fixture: ComponentFixture<AdminNutryIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNutryIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNutryIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
