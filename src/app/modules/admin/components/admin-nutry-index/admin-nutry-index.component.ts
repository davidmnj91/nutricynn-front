import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';


@Component({
    selector: 'app-admin-nutry-index',
    templateUrl: './admin-nutry-index.component.html',
    styleUrls: ['./admin-nutry-index.component.scss']
})
export class AdminNutryIndexComponent implements OnInit {

    title: Observable<string>;

    constructor(private router: Router, private route: ActivatedRoute) {
    }

    public ngOnInit() {
        this.title = this.router.events
            .filter(e => e instanceof NavigationEnd)
            .map(() => {
                let route = this.route;
                while (route.firstChild) {
                    route = route.firstChild;
                }
                return route;
            })
            .filter((route) => route.outlet === 'primary')
            .mergeMap((route) => route.data)
            .map(data => data['title']);
    }

}
