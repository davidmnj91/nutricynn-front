import { ComponentRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';


export interface PanelInterface {

    openDialog(any: any): void;

    onRowSelect(event): void;
}
