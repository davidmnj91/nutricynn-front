import { Component, OnInit } from '@angular/core';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { EventCategoryService, DialogService, EditorService } from '@modules/global/services/index.services';
import { EventCategory } from '@models/index.models';
import { Observable } from 'rxjs/Observable';
import { AdminEventCategoriesEditorComponent } from '@modules/admin/components/editors/index.editors';

@Component({
  selector: 'app-admin-event-categories-panel',
  templateUrl: './admin-event-categories-panel.component.html',
  styleUrls: ['./admin-event-categories-panel.component.scss']
})
export class AdminEventCategoriesPanelComponent extends AbstractPanel<EventCategory> {

datas: Observable<EventCategory[]>;

constructor(
    protected service: EventCategoryService,
    protected editorService: EditorService<EventCategory>,
    protected dialogService: DialogService
) {
    super(service, editorService, dialogService, AdminEventCategoriesEditorComponent);
}
}

