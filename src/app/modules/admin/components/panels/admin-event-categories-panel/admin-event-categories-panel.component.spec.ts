import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEventCategoriesPanelComponent } from './admin-event-categories-panel.component';

describe('AdminEventCategoriesPanelComponent', () => {
  let component: AdminEventCategoriesPanelComponent;
  let fixture: ComponentFixture<AdminEventCategoriesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEventCategoriesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEventCategoriesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
