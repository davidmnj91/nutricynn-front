import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDietPanelComponent } from './admin-diet-panel.component';

describe('AdminDietPanelComponent', () => {
  let component: AdminDietPanelComponent;
  let fixture: ComponentFixture<AdminDietPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDietPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDietPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
