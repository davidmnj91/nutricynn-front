import { Component } from '@angular/core';
import { Diet } from '@models/index.models';
import { AdminDietEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { DietService, EditorService, DialogService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'app-admin-diet-panel',
    templateUrl: './admin-diet-panel.component.html',
    styleUrls: ['./admin-diet-panel.component.scss']
})
export class AdminDietPanelComponent extends AbstractPanel<Diet> {

    datas: Observable<Diet[]>;

    constructor(
        protected service: DietService,
        protected editorService: EditorService<Diet>,
        protected dialogService: DialogService
    ) {
        super(service, editorService, dialogService, AdminDietEditorComponent);
    }

    openDialog(model?: Diet): void {
        model = model ? model : new Diet();
        super.openDialog(model);
    }
}

