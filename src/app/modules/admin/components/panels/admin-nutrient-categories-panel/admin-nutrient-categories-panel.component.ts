import { Component } from '@angular/core';
import { NutrientCategory } from '@models/index.models';
import { AdminNutrientCategoriesEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { EditorService, NutrientCategoryService, DialogService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'app-admin-nutrient-categories-panel',
    templateUrl: './admin-nutrient-categories-panel.component.html',
    styleUrls: ['./admin-nutrient-categories-panel.component.scss']
})
export class AdminNutrientCategoriesPanelComponent extends AbstractPanel<NutrientCategory> {

    datas: Observable<NutrientCategory[]>;

    constructor(
        protected service: NutrientCategoryService,
        protected editorService: EditorService<NutrientCategory>,
        protected dialogService: DialogService
    ) {
        super(service, editorService, dialogService, AdminNutrientCategoriesEditorComponent);
    }
}

