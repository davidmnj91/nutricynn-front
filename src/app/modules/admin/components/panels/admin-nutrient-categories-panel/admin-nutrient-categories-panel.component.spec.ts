import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNutrientCategoriesPanelComponent } from './admin-nutrient-categories-panel.component';

describe('AdminNutrientCategoriesPanelComponent', () => {
  let component: AdminNutrientCategoriesPanelComponent;
  let fixture: ComponentFixture<AdminNutrientCategoriesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNutrientCategoriesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNutrientCategoriesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
