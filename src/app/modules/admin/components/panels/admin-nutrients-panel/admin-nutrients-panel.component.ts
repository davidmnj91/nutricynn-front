import { Component } from '@angular/core';
import { Nutrient } from '@models/index.models';
import { AdminNutrientsEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { DialogService, EditorService, NutrientService } from '@modules/global/services/index.services';


@Component({
    selector: 'app-admin-nutrients-panel',
    templateUrl: './admin-nutrients-panel.component.html',
    styleUrls: ['./admin-nutrients-panel.component.scss']
})
export class AdminNutrientsPanelComponent extends AbstractPanel<Nutrient> {

    constructor(
        protected service: NutrientService,
        protected dialogService: DialogService,
        protected editorService: EditorService<Nutrient>
    ) {
        super(service, editorService, dialogService, AdminNutrientsEditorComponent);
    }
}
