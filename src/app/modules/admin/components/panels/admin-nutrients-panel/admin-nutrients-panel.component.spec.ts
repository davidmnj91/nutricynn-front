import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNutrientsPanelComponent } from './admin-nutrients-panel.component';

describe('AdminNutrientsPanelComponent', () => {
  let component: AdminNutrientsPanelComponent;
  let fixture: ComponentFixture<AdminNutrientsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNutrientsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNutrientsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
