import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticleCategoriesPanelComponent } from './admin-article-categories-panel.component';

describe('AdminArticleCategoriesPanelComponent', () => {
  let component: AdminArticleCategoriesPanelComponent;
  let fixture: ComponentFixture<AdminArticleCategoriesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminArticleCategoriesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminArticleCategoriesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
