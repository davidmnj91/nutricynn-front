import { Component } from '@angular/core';
import { ArticleCategory } from '@models/index.models';
import { AdminArticleCategoriesEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { ArticleCategoryService, DialogService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'app-admin-article-categories-panel',
    templateUrl: './admin-article-categories-panel.component.html',
    styleUrls: ['./admin-article-categories-panel.component.scss'],
    providers: [ArticleCategoryService, EditorService]
})
export class AdminArticleCategoriesPanelComponent extends AbstractPanel<ArticleCategory> {

    datas: Observable<ArticleCategory[]>;

    constructor(
        protected service: ArticleCategoryService,
        protected editorService: EditorService<ArticleCategory>,
        protected dialogService: DialogService
    ) {
        super(service, editorService, dialogService, AdminArticleCategoriesEditorComponent);
    }
}
