import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUnitsPanelComponent } from './admin-units-panel.component';

describe('AdminUnitsPanelComponent', () => {
  let component: AdminUnitsPanelComponent;
  let fixture: ComponentFixture<AdminUnitsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUnitsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUnitsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
