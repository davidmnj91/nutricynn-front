import { Component } from '@angular/core';
import { NutrientUnit } from '@models/index.models';
import { AdminUnitsEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { EditorService, NutrientUnitService, DialogService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'app-admin-units-panel',
    templateUrl: './admin-units-panel.component.html',
    styleUrls: ['./admin-units-panel.component.scss']
})
export class AdminUnitsPanelComponent extends AbstractPanel<NutrientUnit> {

    datas: Observable<NutrientUnit[]>;

    constructor(
        protected service: NutrientUnitService,
        protected editorService: EditorService<NutrientUnit>,
        protected dialogService: DialogService,
    ) {
        super(service, editorService, dialogService, AdminUnitsEditorComponent);
    }
}

