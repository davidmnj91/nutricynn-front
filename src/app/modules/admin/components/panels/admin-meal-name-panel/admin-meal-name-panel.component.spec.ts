import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMealNamePanelComponent } from './admin-meal-name-panel.component';

describe('AdminMealNamePanelComponent', () => {
  let component: AdminMealNamePanelComponent;
  let fixture: ComponentFixture<AdminMealNamePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMealNamePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMealNamePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
