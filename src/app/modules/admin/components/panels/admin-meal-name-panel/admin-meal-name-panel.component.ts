import { Component } from '@angular/core';
import { MealName } from '@models/index.models';
import { AdminMealNameEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { EditorService, MealNameService, DialogService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-admin-meal-name-panel',
    templateUrl: './admin-meal-name-panel.component.html',
    styleUrls: ['./admin-meal-name-panel.component.scss']
})
export class AdminMealNamePanelComponent extends AbstractPanel<MealName> {

    datas: Observable<MealName[]>;

    constructor(
        protected service: MealNameService,
        protected editorService: EditorService<MealName>,
        protected dialogService: DialogService
    ) {
        super(service, editorService, dialogService, AdminMealNameEditorComponent);
    }
}
