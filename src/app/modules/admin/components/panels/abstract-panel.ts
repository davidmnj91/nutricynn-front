import { ComponentType } from '@angular/cdk/overlay';
import { TemplateRef } from '@angular/core';
import { AbstractModel } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/index.editors';
import { AbstractService, DialogService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


export abstract class AbstractPanel<M extends AbstractModel> {

    datas: Observable<M[]>;

    constructor(
        protected service: AbstractService<M>,
        protected editorService: EditorService<M>,
        protected dialogService: DialogService,
        protected editor: ComponentType<AbstractEditor<M>> | TemplateRef<AbstractEditor<M>>
    ) {
        this.datas = service.data$;
    }

    openDialog(model?: M): void {
        this.editorService.show(this.editor, model).subscribe(result => {
            if (result) {
                this.service.post(result);
            }
        });
    }

    onRowSelect(event: any): void {
        this.openDialog(event.data);
    }

    deleteItem(item: M) {
        this.dialogService.confirm('Delete confirm', 'Are you sure you want to delete item?', item)
            .subscribe(res => {
                if (res) {
                    this.service.delete(item.id);
                }
            });
    }
}
