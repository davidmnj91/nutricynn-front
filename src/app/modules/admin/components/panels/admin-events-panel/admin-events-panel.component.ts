import { Component, OnInit } from '@angular/core';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { EventService, DialogService, EditorService } from '@modules/global/services/index.services';
import { Event } from '@models/index.models';
import { Observable } from 'rxjs/Observable';
import { AdminEventEditorComponent } from '@modules/admin/components/editors/index.editors';

@Component({
  selector: 'app-admin-events-panel',
  templateUrl: './admin-events-panel.component.html',
  styleUrls: ['./admin-events-panel.component.scss']
})
export class AdminEventsPanelComponent extends AbstractPanel<Event> {

  datas: Observable<Event[]>;

  constructor(
      protected service: EventService,
      protected editorService: EditorService<Event>,
      protected dialogService: DialogService
  ) {
      super(service, editorService, dialogService, AdminEventEditorComponent);
  }
}
