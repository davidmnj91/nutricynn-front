import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEventsPanelComponent } from './admin-events-panel.component';

describe('AdminEventPanelComponent', () => {
  let component: AdminEventsPanelComponent;
  let fixture: ComponentFixture<AdminEventsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEventsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEventsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
