import { Component } from '@angular/core';
import { Article } from '@models/index.models';
import { AdminArticlesEditorComponent } from '@modules/admin/components/editors/index.editors';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { ArticleService, EditorService, DialogService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-admin-articles-panel',
    templateUrl: './admin-articles-panel.component.html',
    styleUrls: ['./admin-articles-panel.component.scss']
})
export class AdminArticlesPanelComponent extends AbstractPanel<Article> {

    datas: Observable<Article[]>;

    constructor(
        protected service: ArticleService,
        protected editorService: EditorService<Article>,
        protected dialogService: DialogService
    ) {
        super(service, editorService, dialogService, AdminArticlesEditorComponent);
    }
}
