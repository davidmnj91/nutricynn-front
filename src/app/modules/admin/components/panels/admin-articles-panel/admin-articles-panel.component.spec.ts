import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticlesPanelComponent } from './admin-articles-panel.component';

describe('AdminArticlesPanelComponent', () => {
  let component: AdminArticlesPanelComponent;
  let fixture: ComponentFixture<AdminArticlesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminArticlesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminArticlesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
