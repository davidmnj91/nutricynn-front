import { Component } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { AbstractPanel } from '@modules/admin/components/panels/abstract-panel';
import { DialogService, EditorService, UserAccountService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';
import { AdminUserEditorComponent } from '@modules/admin/components/editors/index.editors';

@Component({
    selector: 'app-admin-user-panel',
    templateUrl: './admin-user-panel.component.html',
    styleUrls: ['./admin-user-panel.component.scss']
})
export class AdminUserPanelComponent extends AbstractPanel<UserAccount> {

    datas: Observable<UserAccount[]>;

    constructor(
        protected service: UserAccountService,
        protected editorService: EditorService<UserAccount>,
        protected dialogService: DialogService,
    ) {
        super(service, editorService, dialogService, AdminUserEditorComponent);
    }
}
