import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DietFood, Nutrient, NutrientQuantity, NutrientUnit } from '@models/index.models';
import { NutrientService, NutrientUnitService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-admin-diet-nutrient-editor',
    templateUrl: './admin-diet-nutrient-editor.component.html',
    styleUrls: ['./admin-diet-nutrient-editor.component.scss']
})
export class AdminDietNutrientEditorComponent {

    @Input()
    food: DietFood;

    nutrients: Observable<Nutrient[]>;
    units: Observable<NutrientUnit[]>;

    nutrientForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private unitService: NutrientUnitService,
        private nutrientService: NutrientService
    ) {
        this.nutrients = nutrientService.data$;
        this.units = unitService.data$;
        this.nutrientForm = formBuilder.group({
            'id': [],
            'nutrient': [null, Validators.required],
            'quantity': ['', Validators.required],
            'unit': [null, Validators.required]
        });
    }

    addNutrient() {
        if (this.nutrientForm.valid) {
            const nutrients: NutrientQuantity = this.nutrientForm.value;
            if (this.food.nutrients == null) {
                this.food.nutrients = new Array<NutrientQuantity>();
            }
            this.food.nutrients.push(nutrients);
            this.nutrientForm.reset();
            console.log(this.food);
        }
    }

    deleteNutrient(nutrient: NutrientQuantity) {
        const idNutrient: number = this.food.nutrients.indexOf(nutrient);
        this.food.nutrients.splice(idNutrient, 1);
    }

}
