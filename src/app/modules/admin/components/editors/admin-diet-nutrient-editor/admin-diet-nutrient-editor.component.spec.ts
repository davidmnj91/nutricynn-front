import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDietNutrientEditorComponent } from './admin-diet-nutrient-editor.component';

describe('AdminDietNutrientEditorComponent', () => {
  let component: AdminDietNutrientEditorComponent;
  let fixture: ComponentFixture<AdminDietNutrientEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDietNutrientEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDietNutrientEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
