import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNutrientsEditorComponent } from './admin-nutrients-editor.component';

describe('AdminNutrientsEditorComponent', () => {
  let component: AdminNutrientsEditorComponent;
  let fixture: ComponentFixture<AdminNutrientsEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNutrientsEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNutrientsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
