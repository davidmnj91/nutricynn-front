import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NutrientCategory } from '@models/index.models';
import { Nutrient } from '@models/nutrient';
import { EditorService, NutrientCategoryService, NutrientService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';


@Component({
    selector: 'app-admin-nutrients-editor',
    templateUrl: './admin-nutrients-editor.component.html',
    styleUrls: ['./admin-nutrients-editor.component.scss']
})
export class AdminNutrientsEditorComponent extends AbstractEditor<Nutrient> {

    title = 'Nutrient';
    categories: Observable<NutrientCategory[]>;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: Nutrient,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<Nutrient>,
        private categoryService: NutrientCategoryService,
        private nutrientService: NutrientService
    ) {
        super(model, editorService);

        this.categories = categoryService.data$;
        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'icon': ['', Validators.required],
            'category': ['', Validators.required],
            'energy': ['', Validators.required],
            'carbs': ['', Validators.required],
            'fats': ['', Validators.required],
            'proteins': ['', Validators.required]
        });
    }
}
