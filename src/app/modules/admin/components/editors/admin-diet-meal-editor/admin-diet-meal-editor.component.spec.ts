import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDietMealEditorComponent } from './admin-diet-meal-editor.component';

describe('AdminDietMealEditorComponent', () => {
  let component: AdminDietMealEditorComponent;
  let fixture: ComponentFixture<AdminDietMealEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDietMealEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDietMealEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
