import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DietDay, DietMeal, MealName } from '@models/index.models';
import { MealNameService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-admin-diet-meal-editor',
    templateUrl: './admin-diet-meal-editor.component.html',
    styleUrls: ['./admin-diet-meal-editor.component.scss']
})
export class AdminDietMealEditorComponent {

    @Input()
    day: DietDay;

    mealNames: Observable<MealName[]>;
    mealNameForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private service: MealNameService) {
        this.mealNames = service.data$;
        this.mealNameForm = formBuilder.group({
            'id': [],
            'mealName': [null, Validators.required],
            'time': ['', Validators.required]
        });
    }

    addMeal() {
        if (this.mealNameForm.valid) {
            const dietMeal: DietMeal = this.mealNameForm.value;
            if (this.day.meals == null) { this.day.meals = new Array<DietMeal>(); }
            this.day.meals.push(dietMeal);
            this.mealNameForm.reset();
            console.log(this.day);
        }
    }

    deleteMeal(meal: DietMeal) {
        const idMeal: number = this.day.meals.indexOf(meal);
        this.day.meals.splice(idMeal, 1);
    }

}
