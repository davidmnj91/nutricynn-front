import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MealName } from '@models/index.models';
import { EditorService, MealNameService } from '@modules/global/services/index.services';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';


@Component({
    selector: 'app-admin-meal-name-editor',
    templateUrl: './admin-meal-name-editor.component.html',
    styleUrls: ['./admin-meal-name-editor.component.scss']
})
export class AdminMealNameEditorComponent extends AbstractEditor<MealName> {

    title = 'Meal Name';

    constructor(
        @Inject(MAT_DIALOG_DATA) model: MealName,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<MealName>,
        private mealNameService: MealNameService
    ) {
        super(model, editorService);
        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required]
        });
    }
}
