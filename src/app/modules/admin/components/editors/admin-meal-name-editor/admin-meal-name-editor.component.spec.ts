import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMealNameEditorComponent } from './admin-meal-name-editor.component';

describe('AdminMealNameEditorComponent', () => {
  let component: AdminMealNameEditorComponent;
  let fixture: ComponentFixture<AdminMealNameEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMealNameEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMealNameEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
