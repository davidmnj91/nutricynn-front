import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEventCategoriesEditorComponent } from './admin-event-categories-editor.component';

describe('AdminEventCategoriesEditorComponent', () => {
  let component: AdminEventCategoriesEditorComponent;
  let fixture: ComponentFixture<AdminEventCategoriesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEventCategoriesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEventCategoriesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
