import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { EditorService, EventCategoryService } from '@modules/global/services/index.services';
import { EventCategory } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';

@Component({
  selector: 'app-admin-event-categories-editor',
  templateUrl: './admin-event-categories-editor.component.html',
  styleUrls: ['./admin-event-categories-editor.component.scss']
})
export class AdminEventCategoriesEditorComponent extends AbstractEditor<EventCategory> {

  title = 'Event Category';
  categories: Observable<EventCategory[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA) model: EventCategory,
    private formBuilder: FormBuilder,
    protected editorService: EditorService<EventCategory>,
    private categoryService: EventCategoryService
  ) {
    super(model, editorService);
    this.categories = categoryService.data$;

    this.form = formBuilder.group({
      'id': [],
      'name': ['', Validators.required],
      'fatherCategory': []
    });
  }
}

