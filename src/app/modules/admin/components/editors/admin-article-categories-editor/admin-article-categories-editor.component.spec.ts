import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticleCategoriesEditorComponent } from './admin-article-categories-editor.component';

describe('AdminArticleCategoriesEditorComponent', () => {
  let component: AdminArticleCategoriesEditorComponent;
  let fixture: ComponentFixture<AdminArticleCategoriesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminArticleCategoriesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminArticleCategoriesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
