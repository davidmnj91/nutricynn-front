import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ArticleCategory } from '@models/index.models';
import { ArticleCategoryService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';

@Component({
    selector: 'app-admin-article-categories-editor',
    templateUrl: './admin-article-categories-editor.component.html',
    styleUrls: ['./admin-article-categories-editor.component.scss']
})
export class AdminArticleCategoriesEditorComponent extends AbstractEditor<ArticleCategory> {

    title = 'Article Category';
    categories: Observable<ArticleCategory[]>;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: ArticleCategory,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<ArticleCategory>,
        private categoryService: ArticleCategoryService
    ) {
        super(model, editorService);
        this.categories = categoryService.data$;

        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'fatherCategory': []
        });
    }
}
