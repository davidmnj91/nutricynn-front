import { ComponentType } from '@angular/cdk/overlay';
import { Input } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { AbstractModel } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/index.editors';
import { AbstractService, EditorService } from '@modules/global/services/index.services';


export abstract class AbstractChildPanel<C extends AbstractModel> {

    @Input()
    model: C;

    constructor(
        protected service: AbstractService<C>,
        protected editorService: EditorService<C>,
        protected editor: ComponentType<AbstractEditor<C>> | TemplateRef<AbstractEditor<C>>
    ) { }

    openDialog(): void {
        this.editorService.show(this.editor, this.model).subscribe(result => {
            if (result) {
                this.service.post(result);
                this.model = result;
            }
        });
    }
}
