import { Component } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { AbstractChildPanel } from '@modules/admin/components/editors/user/abstract-child-panel';
import {
  UserProfileEditorComponent,
} from '@modules/admin/components/editors/user/user-profile-editor/user-profile-editor.component';
import { EditorService, UserAccountService } from '@modules/global/services/index.services';

@Component({
  selector: 'app-user-polls-tab',
  templateUrl: './user-polls-tab.component.html',
  styleUrls: ['./user-polls-tab.component.scss']
})
export class UserPollsTabComponent extends AbstractChildPanel<UserAccount> {

  constructor(
    service: UserAccountService,
    editorService: EditorService<UserAccount>,
  ) {
    super(service, editorService, UserProfileEditorComponent);
  }

  updateBasicData($event): void {
    this.model.userPolls.pollBasicData = $event;
    this.service.post(this.model);
  }

  updateClinicData($event): void {
    this.model.userPolls.pollClinicData = $event;
    this.service.post(this.model);
  }

  updateWeekDiet($event): void {
    this.model.userPolls.pollWeekDiet = $event;
    this.service.post(this.model);
  }

  updateWeekEndDiet($event): void {
    this.model.userPolls.pollWeekEndDiet = $event;
    this.service.post(this.model);
  }

}

