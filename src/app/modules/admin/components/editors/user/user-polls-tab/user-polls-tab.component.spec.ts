import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPollsTabComponent } from './user-polls-tab.component';

describe('UserPollsTabComponent', () => {
  let component: UserPollsTabComponent;
  let fixture: ComponentFixture<UserPollsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPollsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPollsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
