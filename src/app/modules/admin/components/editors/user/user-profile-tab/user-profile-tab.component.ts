import { Component } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { AbstractChildPanel } from '@modules/admin/components/editors/user/abstract-child-panel';
import {
  UserProfileEditorComponent,
} from '@modules/admin/components/editors/user/user-profile-editor/user-profile-editor.component';
import { EditorService, UserAccountService } from '@modules/global/services/index.services';

@Component({
  selector: 'app-user-profile-tab',
  templateUrl: './user-profile-tab.component.html',
  styleUrls: ['./user-profile-tab.component.scss']
})
export class UserProfileTabComponent extends AbstractChildPanel<UserAccount> {

  constructor(
    service: UserAccountService,
    editorService: EditorService<UserAccount>,
  ) {
    super(service, editorService, UserProfileEditorComponent);
   }

}
