import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDietEditorComponent } from './user-diet-editor.component';

describe('UserDietEditorComponent', () => {
  let component: UserDietEditorComponent;
  let fixture: ComponentFixture<UserDietEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDietEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDietEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
