import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Diet, DietProgress, UserDiet } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';
import { DietService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user-diet-editor',
  templateUrl: './user-diet-editor.component.html',
  styleUrls: ['./user-diet-editor.component.scss']
})
export class UserDietEditorComponent extends AbstractEditor<UserDiet> {

  title = 'Diet';
  diets: Observable<Diet[]>;

  today = Date.now();

  constructor(
    @Inject(MAT_DIALOG_DATA) model: UserDiet,
    protected editorService: EditorService<UserDiet>,
    private formBuilder: FormBuilder,
    private dietService: DietService
  ) {
    super(model ? model : new UserDiet(), editorService);

    this.diets = dietService.data$;
    this.form = formBuilder.group({
      'userAccount': [],
      'startDate': [Date.now(), Validators.required],
      'endDate': [Date.now(), Validators.required],
      'diet': [new Diet(), Validators.required],
      'dietProgress': [new Array<DietProgress>()]
    });
  }

}
