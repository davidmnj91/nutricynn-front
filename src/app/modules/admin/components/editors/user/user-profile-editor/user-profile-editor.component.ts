import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { UserAccount } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';
import { EditorService } from '@modules/global/services/index.services';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-user-profile-editor',
  templateUrl: './user-profile-editor.component.html',
  styleUrls: ['./user-profile-editor.component.scss']
})
export class UserProfileEditorComponent extends AbstractEditor<UserAccount> {

  constructor(
    @Inject(MAT_DIALOG_DATA) model: UserAccount,
    private formBuilder: FormBuilder,
    protected editorService: EditorService<UserAccount>
  ) {
    super(model, editorService);

    this.form = formBuilder.group({
      'id': [],
      'username': ['', Validators.required],
      'email': ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
      'password': ['', Validators.required],
      'roles': [null, Validators.required],
      'enabled': [true, Validators.required],
      'name': ['', Validators.required],
      'lastName': ['', Validators.required],
      'photoPath': ['', Validators.required],
      'birthday': ['', Validators.required],
      'phone': ['', Validators.required],
      'address': ['', Validators.required],
      'weight': ['', Validators.required],
      'height': ['', Validators.required]
    });
  }

}
