import { Component, Input } from '@angular/core';
import { UserDiet } from '@models/index.models';
import { UserDietEditorComponent } from '@modules/admin/components/editors/user/user-diet-editor/user-diet-editor.component';
import { EditorService, UserDietService } from '@modules/global/services/index.services';

@Component({
  selector: 'app-user-diet-history-tab',
  templateUrl: './user-diet-history-tab.component.html',
  styleUrls: ['./user-diet-history-tab.component.scss']
})
export class UserDietHistoryTabComponent {

  @Input()
  model: UserDiet[];

  constructor(
    public service: UserDietService,
    public editorService: EditorService<UserDiet>,
  ) {  }

  openDialog(model?: UserDiet): void {
    this.editorService.show(UserDietEditorComponent, model).subscribe(result => {
        if (result) {
            this.service.post(result);
            this.model = [...this.model, result];
        }
    });
}

}
