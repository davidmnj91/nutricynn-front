import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDietHistoryTabComponent } from './user-diet-history-tab.component';

describe('UserDietHistoryTabComponent', () => {
  let component: UserDietHistoryTabComponent;
  let fixture: ComponentFixture<UserDietHistoryTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDietHistoryTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDietHistoryTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
