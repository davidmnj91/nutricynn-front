import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEventEditorComponent } from './admin-event-editor.component';

describe('AdminEventEditorComponent', () => {
  let component: AdminEventEditorComponent;
  let fixture: ComponentFixture<AdminEventEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEventEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEventEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
