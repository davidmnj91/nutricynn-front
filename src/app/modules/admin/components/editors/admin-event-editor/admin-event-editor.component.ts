import { Component, OnInit, Inject } from '@angular/core';
import { EventService, EventCategoryService, EditorService } from '@modules/global/services/index.services';
import { EventCategory, Event } from '@models/index.models';
import { Observable } from 'rxjs/Observable';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Validators, FormBuilder } from '@angular/forms';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';

@Component({
    selector: 'app-admin-event-editor',
    templateUrl: './admin-event-editor.component.html',
    styleUrls: ['./admin-event-editor.component.scss']
})
export class AdminEventEditorComponent extends AbstractEditor<Event> {

    title = 'Event';
    categories: Observable<EventCategory[]>;
    text: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: Event,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<Event>,
        private categoryService: EventCategoryService,
        private eventService: EventService
    ) {
        super(model, editorService);

        this.categories = categoryService.data$;
        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'category': [null, Validators.required],
            'content': ['', Validators.required]
        });
    }

    onSubmit(): void {
        const type: Event = this.form.value;
        this.eventService.post(type);
        // this.editorService.close();
    }
}
