import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AbstractModel } from '@models/index.models';
import { EditorService } from '@modules/global/services/index.services';


export abstract class AbstractEditor<M extends AbstractModel> implements OnInit {

    form: FormGroup;

    constructor(public data: M, protected editorService: EditorService<M>) { }

    public ngOnInit(): void {
        if (this.data && this.form) {
            Object.keys(this.data).forEach(k => {
                const control = this.form.get(k);
                if (control) {
                    control.setValue(this.data[k], { onlySelf: true });
                }
            });
        }
    }

    onSubmit(): void {
        this.editorService.close(this.form.value);
    }

    onCancel(): void {
        this.editorService.close();
    }
}
