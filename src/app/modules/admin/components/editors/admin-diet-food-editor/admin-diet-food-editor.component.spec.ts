import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDietFoodEditorComponent } from './admin-diet-food-editor.component';

describe('AdminDietFoodEditorComponent', () => {
  let component: AdminDietFoodEditorComponent;
  let fixture: ComponentFixture<AdminDietFoodEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDietFoodEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDietFoodEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
