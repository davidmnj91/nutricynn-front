import { Component, Input } from '@angular/core';
import { DietMeal, DietFood, MealName } from '@models/index.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MealNameService, EditorService } from '@modules/global/services/index.services';

@Component({
    selector: 'app-admin-diet-food-editor',
    templateUrl: './admin-diet-food-editor.component.html',
    styleUrls: ['./admin-diet-food-editor.component.scss']
})
export class AdminDietFoodEditorComponent {

    @Input()
    meal: DietMeal;
    foodForm: FormGroup;


    constructor(private formBuilder: FormBuilder, mealNameService: MealNameService) {
        this.foodForm = formBuilder.group({
            'id': [],
            'name': [null, Validators.required],
            'nutrients': [null]
        });
    }

    addFood() {
        if (this.foodForm.valid) {
            const dietFood: DietFood = this.foodForm.value;
            if (this.meal.foods == null) {
                this.meal.foods = new Array<DietFood>();
            }
            this.meal.foods.push(dietFood);
            this.foodForm.reset();
            console.log(this.meal);
        }
    }

    deleteFood(food: DietFood) {
        const idFood: number = this.meal.foods.indexOf(food);
        this.meal.foods.splice(idFood, 1);
    }

}
