import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { UserAccount, UserDiet } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';
import { EditorService, UserDietService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-admin-user-editor',
  templateUrl: './admin-user-editor.component.html',
  styleUrls: ['./admin-user-editor.component.scss']
})
export class AdminUserEditorComponent extends AbstractEditor<UserAccount> {

  title = 'User';

  diets$: Observable<UserDiet[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA) model: UserAccount,
    private formBuilder: FormBuilder,
    protected editorService: EditorService<UserAccount>,
    private userDietService: UserDietService
  ) {
    super(model, editorService);
    userDietService.getByUsername(model.username);
    this.diets$ = userDietService.data$;
  }
}

