import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NutrientCategory } from '@models/index.models';
import { EditorService, NutrientCategoryService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';

@Component({
    selector: 'app-admin-nutrient-categories-editor',
    templateUrl: './admin-nutrient-categories-editor.component.html',
    styleUrls: ['./admin-nutrient-categories-editor.component.scss']
})
export class AdminNutrientCategoriesEditorComponent extends AbstractEditor<NutrientCategory> {

    title = 'Nutrient Category';
    categories: Observable<NutrientCategory[]>;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: NutrientCategory,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<NutrientCategory>,
        private categoryService: NutrientCategoryService
    ) {
        super(model, editorService);
        this.categories = categoryService.data$;

        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'fatherCategory': []
        });
    }
}
