import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNutrientCategoriesEditorComponent } from './admin-nutrient-categories-editor.component';

describe('AdminNutrientCategoriesEditorComponent', () => {
  let component: AdminNutrientCategoriesEditorComponent;
  let fixture: ComponentFixture<AdminNutrientCategoriesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNutrientCategoriesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNutrientCategoriesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
