import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Article, ArticleCategory } from '@models/index.models';
import { ArticleCategoryService, ArticleService, EditorService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';


@Component({
    selector: 'app-admin-articles-editor',
    templateUrl: './admin-articles-editor.component.html',
    styleUrls: ['./admin-articles-editor.component.scss']
})
export class AdminArticlesEditorComponent extends AbstractEditor<Article> {

    title = 'Article';
    categories: Observable<ArticleCategory[]>;
    text: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: Article,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<Article>,
        private categoryService: ArticleCategoryService,
        private articleService: ArticleService
    ) {
        super(model, editorService);

        this.categories = categoryService.data$;
        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'category': [null, Validators.required],
            'content': ['', Validators.required]
        });
    }

    onSubmit(): void {
        const type: Article = this.form.value;
        this.articleService.post(type);
        // this.editorService.close();
    }
}
