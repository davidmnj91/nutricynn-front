import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticlesEditorComponent } from './admin-articles-editor.component';

describe('AdminArticlesEditorComponent', () => {
  let component: AdminArticlesEditorComponent;
  let fixture: ComponentFixture<AdminArticlesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminArticlesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminArticlesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
