import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUnitsEditorComponent } from './admin-units-editor.component';

describe('AdminUnitsEditorComponent', () => {
  let component: AdminUnitsEditorComponent;
  let fixture: ComponentFixture<AdminUnitsEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUnitsEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUnitsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
