import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NutrientUnit } from '@models/index.models';
import { EditorService, NutrientUnitService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Rx';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';


@Component({
    selector: 'app-admin-units-editor',
    templateUrl: './admin-units-editor.component.html',
    styleUrls: ['./admin-units-editor.component.scss']
})
export class AdminUnitsEditorComponent extends AbstractEditor<NutrientUnit> {

    title = 'Nutrient Unit';
    units: Observable<NutrientUnit[]>;

    constructor(
        @Inject(MAT_DIALOG_DATA) data: any,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<NutrientUnit>,
        private unitService: NutrientUnitService
    ) {
        super(data, editorService);

        this.units = unitService.data$;
        this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'shortName': ['', Validators.required],
            'conversion': [''],
            'baseUnit': [],
            'icon': ['', Validators.required]
        });
    }
}
