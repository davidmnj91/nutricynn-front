import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Diet, WeekDays } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/editors/abstract-editor';
import { EditorService } from '@modules/global/services/index.services';


@Component({
    selector: 'app-admin-diet-editor',
    templateUrl: './admin-diet-editor.component.html',
    styleUrls: ['./admin-diet-editor.component.scss'],
})
export class AdminDietEditorComponent extends AbstractEditor<Diet> {

    title = 'Diet';
    days = WeekDays;

    constructor(
        @Inject(MAT_DIALOG_DATA) model: Diet,
        private formBuilder: FormBuilder,
        protected editorService: EditorService<Diet>
    ) {
        super(model, editorService);
        this.form = this.form = formBuilder.group({
            'id': [],
            'name': ['', Validators.required],
            'description': [''],
            'baseDiet': [true]
        });
    }

    onSubmit(): void {
        this.data.name = this.form.controls.name.value;
        this.data.description = this.form.controls.description.value;
        this.editorService.close(this.data);
    }
}
