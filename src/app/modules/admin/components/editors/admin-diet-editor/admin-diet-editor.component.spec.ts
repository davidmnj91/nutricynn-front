import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDietEditorComponent } from './admin-diet-editor.component';

describe('AdminDietEditorComponent', () => {
  let component: AdminDietEditorComponent;
  let fixture: ComponentFixture<AdminDietEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDietEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDietEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
