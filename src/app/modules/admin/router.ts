import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    AdminMainComponent, AdminUserPanelComponent, AdminDietPanelComponent, AdminNutrientsPanelComponent, AdminUnitsPanelComponent,
    AdminNutrientCategoriesPanelComponent, AdminMealNamePanelComponent, AdminArticlesPanelComponent, AdminArticleCategoriesPanelComponent,
    AdminEventsPanelComponent, AdminEventCategoriesPanelComponent
} from './components/index.components';

const adminRoutes: Routes = [
    {
        path: '', component: AdminMainComponent,
        children: [
            { path: '', redirectTo: 'users', pathMatch: 'full', data: { title: 'Users' } },
            { path: 'users', component: AdminUserPanelComponent, data: { title: 'Users' } },
            { path: 'diet', component: AdminDietPanelComponent, data: { title: 'Diets' } },
            { path: 'mealName', component: AdminMealNamePanelComponent, data: { title: 'Meal Names' } },
            { path: 'nutrient', component: AdminNutrientsPanelComponent, data: { title: 'Nutrients' } },
            { path: 'nutrientcategories', component: AdminNutrientCategoriesPanelComponent, data: { title: 'Nutrient Categories' } },
            { path: 'unit', component: AdminUnitsPanelComponent, data: { title: 'Units' } },
            { path: 'articles', component: AdminArticlesPanelComponent, data: { title: 'Articles' } },
            { path: 'articlecategories', component: AdminArticleCategoriesPanelComponent, data: { title: 'Article Categories' } },
            { path: 'events', component: AdminEventsPanelComponent, data: { title: 'Events' } },
            { path: 'eventcategories', component: AdminEventCategoriesPanelComponent, data: { title: 'Event Categories' } },
        ]
    }
];

export const AdminRouter: ModuleWithProviders = RouterModule.forChild(adminRoutes);
