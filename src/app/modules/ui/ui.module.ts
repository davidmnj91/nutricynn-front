import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { GlobalModule } from '@modules/global/global.module';

import {
  ConfirmDialogComponent,
  DietDayComponent,
  DietFoodComponent,
  ErrorNotificationComponent,
  LoginComponent,
  MainFooterComponent,
  PollBasicDataEditorComponent,
  PollBasicDataFormComponent,
  PollBasicDataPanelComponent,
  PollClinicDataEditorComponent,
  PollClinicDataFormComponent,
  PollClinicDataPanelComponent,
  PollWeekDietEditorComponent,
  PollWeekDietFormComponent,
  PollWeekDietPanelComponent,
  PollWeekEndDietEditorComponent,
  PollWeekEndDietFormComponent,
  PollWeekEndDietPanelComponent,
  RegisterComponent,
  UserDietHistoryComponent,
  UserPollCardComponent,
  UserProfileComponent,
} from './components/index.components';

@NgModule({
  imports: [
    GlobalModule,
    CommonModule
  ],
  declarations: [
    DietFoodComponent,
    DietDayComponent,
    UserPollCardComponent,
    UserDietHistoryComponent,
    UserProfileComponent,
    PollClinicDataPanelComponent,
    PollBasicDataPanelComponent,
    PollWeekEndDietPanelComponent,
    PollWeekDietPanelComponent,
    PollBasicDataEditorComponent,
    PollClinicDataEditorComponent,
    PollWeekEndDietEditorComponent,
    PollWeekDietEditorComponent,
    ConfirmDialogComponent,
    ErrorNotificationComponent,
    LoginComponent,
    MainFooterComponent,
    RegisterComponent,
    PollBasicDataFormComponent,
    PollClinicDataFormComponent,
    PollWeekDietFormComponent,
    PollWeekEndDietFormComponent
  ],
  exports: [
    DietFoodComponent,
    DietDayComponent,
    UserPollCardComponent,
    UserDietHistoryComponent,
    UserProfileComponent,
    PollClinicDataPanelComponent,
    PollBasicDataPanelComponent,
    PollWeekEndDietPanelComponent,
    PollWeekDietPanelComponent,
    PollBasicDataEditorComponent,
    PollClinicDataEditorComponent,
    PollWeekEndDietEditorComponent,
    PollWeekDietEditorComponent,
    ConfirmDialogComponent,
    ErrorNotificationComponent,
    LoginComponent,
    MainFooterComponent,
    RegisterComponent,
    PollBasicDataFormComponent,
    PollClinicDataFormComponent,
    PollWeekDietFormComponent,
    PollWeekEndDietFormComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    RegisterComponent,
    LoginComponent,
    PollBasicDataEditorComponent,
    PollClinicDataEditorComponent,
    PollWeekEndDietEditorComponent,
    PollWeekDietEditorComponent
  ]
})
export class UiModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UiModule
    };
  }
}
