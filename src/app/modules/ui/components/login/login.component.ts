import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from '@modules/global/services/authentication/authentication.service';
import { RegisterComponent } from '@modules/ui/components/register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

  redirectUrl: string = null;

  loginForm: FormGroup;
  error: any = null;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoginComponent>,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router) {
    this.loginForm = formBuilder.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  onSubmit() {
    const userName: string = this.loginForm.value.email;
    const password: string = this.loginForm.value.password;
    this.authService.login(userName, password).subscribe(
      data => {
        this.dialogRef.close();
        if (this.redirectUrl) {
          this.router.navigate([this.redirectUrl]);
        }
      },
      err => {
        this.error = err;
        console.log(this.error);
        this.loginForm.reset();
      });
  }

  showRegistry(): void {
    this.dialogRef.close();
    this.dialog.open(RegisterComponent);
  }

}
