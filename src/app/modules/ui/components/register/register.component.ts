import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import {
  PollBasicDataFormComponent,
  PollClinicDataFormComponent,
  PollWeekDietFormComponent,
  PollWeekEndDietFormComponent,
} from '@modules/ui/components/user/polls/index.polls.components';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  accountForm: FormGroup;

  @ViewChild('basicForm') basicDataForm: PollBasicDataFormComponent;
  @ViewChild('clinicForm') clinicDataForm: PollClinicDataFormComponent;
  @ViewChild('weekForm') weekDietForm: PollWeekDietFormComponent;
  @ViewChild('weekEndForm') weekEndDietForm: PollWeekEndDietFormComponent;

  today = Date.now();

  constructor(public dialogRef: MatDialogRef<RegisterComponent>, formBuilder: FormBuilder) {
    this.accountForm = formBuilder.group({
      'username': ['', Validators.required],
      'email': ['', Validators.required, Validators.pattern(EMAIL_REGEX)],
      'password': ['', Validators.required],
      'roles': [''],
      'credentialsExpired': [true],
      'name': ['', Validators.required],
      'lastName': ['', Validators.required],
      'photoPath': ['', Validators.required],
      'birthday': ['', Validators.required],
      'phone': ['', Validators.required],
      'address': ['', Validators.required],
      'weight': ['', Validators.required],
      'height': ['', Validators.required]
    });
  }

  onSave(): void {
  }

}
