import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPollCardComponent } from './user-poll-card.component';

describe('UserPollCardComponent', () => {
  let component: UserPollCardComponent;
  let fixture: ComponentFixture<UserPollCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPollCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPollCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
