import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-poll-card',
  templateUrl: './user-poll-card.component.html',
  styleUrls: ['./user-poll-card.component.scss']
})
export class UserPollCardComponent {

  @Input()
  icon: string;

  @Input()
  title: string;

  constructor() { }

}
