import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PollWeekEndDiet } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { AbstractPollEditor } from '@modules/ui/components/user/polls/editors/abstract-poll-editor';
import { PollWeekEndDietFormComponent } from '@modules/ui/components/user/polls/forms/index.polls.forms';


@Component({
    selector: 'app-poll-week-end-diet-editor',
    templateUrl: './poll-week-end-diet-editor.component.html',
    styleUrls: ['./poll-week-end-diet-editor.component.scss']
})
export class PollWeekEndDietEditorComponent extends AbstractPollEditor<PollWeekEndDiet, PollWeekEndDietFormComponent> {

    constructor(@Inject(MAT_DIALOG_DATA) poll: PollWeekEndDiet, pollService: PollService<PollWeekEndDiet>) {
        super(poll, pollService);
    }
}
