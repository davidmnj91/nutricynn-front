import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekDietEditorComponent } from './poll-week-diet-editor.component';

describe('PollWeekDietComponent', () => {
  let component: PollWeekDietEditorComponent;
  let fixture: ComponentFixture<PollWeekDietEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekDietEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekDietEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
