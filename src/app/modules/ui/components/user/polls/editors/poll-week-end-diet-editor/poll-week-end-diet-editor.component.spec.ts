import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekEndDietComponent } from './poll-user-week-end-diet.component';

describe('PollWeekEndDietComponent', () => {
  let component: PollWeekEndDietComponent;
  let fixture: ComponentFixture<PollWeekEndDietComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekEndDietComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekEndDietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
