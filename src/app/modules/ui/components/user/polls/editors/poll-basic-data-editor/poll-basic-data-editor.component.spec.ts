import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollBasicDataComponent } from './poll-user-basic-data.component';

describe('PollBasicDataComponent', () => {
  let component: PollBasicDataComponent;
  let fixture: ComponentFixture<PollBasicDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollBasicDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollBasicDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
