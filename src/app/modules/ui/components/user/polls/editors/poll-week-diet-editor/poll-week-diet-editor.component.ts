import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PollWeekDiet } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { AbstractPollEditor } from '@modules/ui/components/user/polls/editors/abstract-poll-editor';
import { PollWeekDietFormComponent } from '@modules/ui/components/user/polls/forms/index.polls.forms';


@Component({
    selector: 'app-poll-week-diet-editor',
    templateUrl: './poll-week-diet-editor.component.html',
    styleUrls: ['./poll-week-diet-editor.component.scss']
})
export class PollWeekDietEditorComponent extends AbstractPollEditor<PollWeekDiet, PollWeekDietFormComponent> {

    constructor(@Inject(MAT_DIALOG_DATA) poll: PollWeekDiet, pollService: PollService<PollWeekDiet>) {
        super(poll, pollService);
    }
}
