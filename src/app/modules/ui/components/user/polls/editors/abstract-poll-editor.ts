import { AfterViewInit, ViewChild } from '@angular/core';
import { AbstractPoll } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';


export abstract class AbstractPollEditor<P extends AbstractPoll, F extends AbstractForm<P>> implements AfterViewInit {

    @ViewChild('form') form: F;

    constructor(public poll: P, protected pollService: PollService<P>) { }

    public ngAfterViewInit() {
        // After the view is initialized, this.userProfile will be available
        this.form.ngOnInit();
    }

    onSubmit(): void {
        this.pollService.close(this.form.onSubmit());
    }

    onCancel(): void {
        this.pollService.close();
    }
}
