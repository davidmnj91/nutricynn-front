import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PollBasicData } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { AbstractPollEditor } from '@modules/ui/components/user/polls/editors/abstract-poll-editor';
import { PollBasicDataFormComponent } from '@modules/ui/components/user/polls/forms/index.polls.forms';


@Component({
    selector: 'app-poll-basic-data-editor',
    templateUrl: './poll-basic-data-editor.component.html',
    styleUrls: ['./poll-basic-data-editor.component.scss'],
})
export class PollBasicDataEditorComponent extends AbstractPollEditor<PollBasicData, PollBasicDataFormComponent> {

    constructor(@Inject(MAT_DIALOG_DATA) poll: PollBasicData, pollService: PollService<PollBasicData>) {
        super(poll, pollService);
    }
}
