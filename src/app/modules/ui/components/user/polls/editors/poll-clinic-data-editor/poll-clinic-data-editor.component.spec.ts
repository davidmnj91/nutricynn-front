import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollClinicDataComponent } from './poll-user-clinic-data.component';

describe('PollClinicDataComponent', () => {
  let component: PollClinicDataComponent;
  let fixture: ComponentFixture<PollClinicDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollClinicDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollClinicDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
