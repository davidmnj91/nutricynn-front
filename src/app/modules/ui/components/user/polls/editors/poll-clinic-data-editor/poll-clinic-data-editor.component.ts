import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PollClinicData } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { PollClinicDataFormComponent } from '@modules/ui/components/index.components';
import { AbstractPollEditor } from '@modules/ui/components/user/polls/editors/abstract-poll-editor';


@Component({
    selector: 'app-poll-clinic-data-editor',
    templateUrl: './poll-clinic-data-editor.component.html',
    styleUrls: ['./poll-clinic-data-editor.component.scss']
})
export class PollClinicDataEditorComponent extends AbstractPollEditor<PollClinicData, PollClinicDataFormComponent> {

    constructor(@Inject(MAT_DIALOG_DATA) poll: PollClinicData, pollService: PollService<PollClinicData>) {
        super(poll, pollService);
    }
}
