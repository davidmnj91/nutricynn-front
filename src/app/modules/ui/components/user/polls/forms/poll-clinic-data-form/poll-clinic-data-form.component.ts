import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BiologicalStatus } from '@models/index.models';
import { PollClinicData } from '@models/polls/index.polls';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';


@Component({
  selector: 'app-poll-clinic-data-form',
  templateUrl: './poll-clinic-data-form.component.html',
  styleUrls: ['./poll-clinic-data-form.component.scss']
})
export class PollClinicDataFormComponent extends AbstractForm<PollClinicData> {

  biologicalStatus = BiologicalStatus;

  constructor(formBuilder: FormBuilder) {
    super();

    this.form = formBuilder.group({
      hideRequired: false,
      floatPlaceholder: 'auto',
      'importantIllness': [false, Validators.required],
      'importantIllnessDescription': ['', Validators.required],
      'biologicalStatus': ['', Validators.required],
      'eatingDisorder': [false, Validators.required],
      'medicineRecord': [false, Validators.required],
      'medicineRecordDescription': ['', Validators.required]
    });
  }
}
