import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollBasicDataFormComponent } from './poll-basic-data-form.component';

describe('PollBasicDataFormComponent', () => {
  let component: PollBasicDataFormComponent;
  let fixture: ComponentFixture<PollBasicDataFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollBasicDataFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollBasicDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
