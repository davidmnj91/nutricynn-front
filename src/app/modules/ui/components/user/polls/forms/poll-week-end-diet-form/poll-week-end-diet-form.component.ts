import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CheatTime, FoodType, UnitAmount, WaterAmount } from '@models/index.models';
import { PollWeekEndDiet } from '@models/polls/index.polls';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';

@Component({
  selector: 'app-poll-week-end-diet-form',
  templateUrl: './poll-week-end-diet-form.component.html',
  styleUrls: ['./poll-week-end-diet-form.component.scss']
})
export class PollWeekEndDietFormComponent extends AbstractForm<PollWeekEndDiet> {

  cheatTime = CheatTime;
  unitAmount = UnitAmount;
  foodType = FoodType;
  waterAmount = WaterAmount;

  constructor(formBuilder: FormBuilder) {
    super();

    this.form = formBuilder.group({
      hideRequired: false,
      floatPlaceholder: 'auto',
      'weekendBreakfast': [null, Validators.required],
      'weekendMidDay': [false, Validators.required],
      'weekendMidDayDescription': ['', Validators.required],
      'weekendLunch': [null, Validators.required],
      'weekendSnack': [false, Validators.required],
      'weekendSnackDescription': ['', Validators.required],
      'weekendDinner': [null, Validators.required],
      'weekendBed': [false, Validators.required],
      'weekendBedDescription': ['', Validators.required],
      'weekendWater': [null, Validators.required],
      'weekendCheatFood': [null, Validators.required],
      'weekendTonics': [null, Validators.required],
      'weekendAlcohol': [null, Validators.required],
      'weekendSmoke': [false, Validators.required]
    });
  }

}
