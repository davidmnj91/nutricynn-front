import { Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

export abstract class AbstractForm<Model> implements OnInit {

    form: FormGroup;

    @Input()
    model: Model;

    constructor() { }

    public ngOnInit(): void {
        if (this.model && this.form) {
            Object.keys(this.model).forEach(k => {
                const control = this.form.get(k);
                if (control) {
                    control.setValue(this.model[k], { onlySelf: true });
                }
            });
        }
    }

    public onSubmit(): Model {
        return this.form.value;
    }

}
