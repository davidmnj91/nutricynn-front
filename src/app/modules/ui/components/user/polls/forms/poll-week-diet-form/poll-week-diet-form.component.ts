import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CheatTime, FoodType, UnitAmount, WaterAmount } from '@models/index.models';
import { PollWeekDiet } from '@models/polls/index.polls';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';

@Component({
  selector: 'app-poll-week-diet-form',
  templateUrl: './poll-week-diet-form.component.html',
  styleUrls: ['./poll-week-diet-form.component.scss']
})
export class PollWeekDietFormComponent extends AbstractForm<PollWeekDiet> {

  cheatTime = CheatTime;
  unitAmount = UnitAmount;
  foodType = FoodType;
  waterAmount = WaterAmount;

  constructor(formBuilder: FormBuilder) {
    super();

    this.form = formBuilder.group({
      hideRequired: false,
      floatPlaceholder: 'auto',
      'workingdayBreakfast': [null, Validators.required],
      'workingdayMidDay': [false, Validators.required],
      'workingdayMidDayDescription': ['', Validators.required],
      'workingdayLunch': [null, Validators.required],
      'workingdaySnack': [false, Validators.required],
      'workingdaySnackDescription': ['', Validators.required],
      'workingdayDinner': [null, Validators.required],
      'workingdayBed': [false, Validators.required],
      'workingdayBedDescription': ['', Validators.required],
      'workingdayWater': [null, Validators.required],
      'workingdayCheatFood': [null, Validators.required],
      'workingdayTonics': [null, Validators.required],
      'workingdayAlcohol': [null, Validators.required],
      'workingdaySmoke': [false, Validators.required]
    });
  }


}
