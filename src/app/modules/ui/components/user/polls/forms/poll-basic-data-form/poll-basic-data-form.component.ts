import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ChefAmount, CivilStatus, FitnessGoal, FitnessReason, FoodAmount, SleepHours } from '@models/index.models';
import { PollBasicData } from '@models/polls/index.polls';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';

@Component({
  selector: 'app-poll-basic-data-form',
  templateUrl: './poll-basic-data-form.component.html',
  styleUrls: ['./poll-basic-data-form.component.scss']
})
export class PollBasicDataFormComponent extends AbstractForm<PollBasicData> {

  fitnessReasons = FitnessReason;
  fitnessGoals = FitnessGoal;
  chefAmount = ChefAmount;
  foodAmount = FoodAmount;
  civilStatuss = CivilStatus;
  sleepHours = SleepHours;

  constructor(formBuilder: FormBuilder) {
    super();

    this.form = formBuilder.group({
      hideRequired: false,
      floatPlaceholder: 'auto',
      'fitnessGoal': [null, Validators.required],
      'fitnessReason': [null, Validators.required],
      'physicalActivity': [false, Validators.required],
      'physicalActivityDescription': ['', Validators.required],
      'foodNumber': ['', Validators.required],
      'workInfo': [false, Validators.required],
      'workInfoDescription': ['', Validators.required],
      'civilStatus': [null, Validators.required],
      'familyInfo': [false, Validators.required],
      'homeInfo': [false, Validators.required],
      'cookingInfo': [null, Validators.required],
      'sleepWorkingInfo': ['', Validators.required],
      'sleepWeekendInfo': ['', Validators.required]
    });
  }


}
