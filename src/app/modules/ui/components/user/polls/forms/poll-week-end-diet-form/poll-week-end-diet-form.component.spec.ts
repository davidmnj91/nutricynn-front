import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekEndDietFormComponent } from './poll-week-end-diet-form.component';

describe('PollWeekEndDietFormComponent', () => {
  let component: PollWeekEndDietFormComponent;
  let fixture: ComponentFixture<PollWeekEndDietFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekEndDietFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekEndDietFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
