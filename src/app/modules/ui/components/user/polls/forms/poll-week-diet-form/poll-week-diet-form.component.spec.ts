import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekDietFormComponent } from './poll-week-diet-form.component';

describe('PollWeekDietFormComponent', () => {
  let component: PollWeekDietFormComponent;
  let fixture: ComponentFixture<PollWeekDietFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekDietFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekDietFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
