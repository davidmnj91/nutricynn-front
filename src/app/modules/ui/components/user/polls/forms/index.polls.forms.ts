// export * from './abstract-poll-form';
export * from './poll-clinic-data-form/poll-clinic-data-form.component';
export * from './poll-basic-data-form/poll-basic-data-form.component';
export * from './poll-week-diet-form/poll-week-diet-form.component';
export * from './poll-week-end-diet-form/poll-week-end-diet-form.component';
