import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollClinicDataFormComponent } from './poll-clinic-data-form.component';

describe('PollClinicDataFormComponent', () => {
  let component: PollClinicDataFormComponent;
  let fixture: ComponentFixture<PollClinicDataFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollClinicDataFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollClinicDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
