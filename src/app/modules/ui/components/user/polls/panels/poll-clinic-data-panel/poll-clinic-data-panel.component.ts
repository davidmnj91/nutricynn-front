import { Component } from '@angular/core';
import { PollClinicData } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { PollClinicDataEditorComponent } from '@modules/ui/components/user/polls/editors/index.polls.editors';
import { AbstractPollPanel } from '@modules/ui/components/user/polls/panels/abstract-poll-panel';

@Component({
  selector: 'app-poll-clinic-data-panel',
  templateUrl: './poll-clinic-data-panel.component.html',
  styleUrls: ['./poll-clinic-data-panel.component.scss']
})
export class PollClinicDataPanelComponent extends
  AbstractPollPanel<PollClinicData, PollClinicDataEditorComponent> {

  constructor(protected editorService: PollService<PollClinicData>) {
    super(editorService, PollClinicDataEditorComponent);
  }
}
