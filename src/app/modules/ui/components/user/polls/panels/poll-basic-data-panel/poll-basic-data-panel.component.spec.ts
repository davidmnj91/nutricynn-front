import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollBasicDataPanelComponent } from './poll-basic-data-panel.component';

describe('PollBasicDataPanelComponent', () => {
  let component: PollBasicDataPanelComponent;
  let fixture: ComponentFixture<PollBasicDataPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollBasicDataPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollBasicDataPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
