import { Component } from '@angular/core';
import { PollBasicData } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { PollBasicDataEditorComponent } from '@modules/ui/components/user/polls/editors/index.polls.editors';
import { AbstractPollPanel } from '@modules/ui/components/user/polls/panels/abstract-poll-panel';

@Component({
  selector: 'app-poll-basic-data-panel',
  templateUrl: './poll-basic-data-panel.component.html',
  styleUrls: ['./poll-basic-data-panel.component.scss']
})
export class PollBasicDataPanelComponent extends AbstractPollPanel<PollBasicData, PollBasicDataEditorComponent> {

  constructor(protected editorService: PollService<PollBasicData>) {
    super(editorService, PollBasicDataEditorComponent);
  }
}
