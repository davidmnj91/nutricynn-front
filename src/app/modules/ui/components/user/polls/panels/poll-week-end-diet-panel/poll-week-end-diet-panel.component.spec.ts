import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekEndDietPanelComponent } from './poll-week-end-diet-panel.component';

describe('PollWeekEndDietPanelComponent', () => {
  let component: PollWeekEndDietPanelComponent;
  let fixture: ComponentFixture<PollWeekEndDietPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekEndDietPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekEndDietPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
