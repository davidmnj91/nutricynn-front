import { Component } from '@angular/core';
import { PollWeekDiet } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { PollWeekDietEditorComponent } from '@modules/ui/components/user/polls/editors/index.polls.editors';
import { AbstractPollPanel } from '@modules/ui/components/user/polls/panels/abstract-poll-panel';

@Component({
  selector: 'app-poll-week-diet-panel',
  templateUrl: './poll-week-diet-panel.component.html',
  styleUrls: ['./poll-week-diet-panel.component.scss']
})
export class PollWeekDietPanelComponent extends AbstractPollPanel<PollWeekDiet, PollWeekDietEditorComponent> {

  constructor(protected editorService: PollService<PollWeekDiet>) {
    super(editorService, PollWeekDietEditorComponent);
  }
}
