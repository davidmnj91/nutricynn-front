import { Component } from '@angular/core';
import { PollWeekEndDiet } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/modal/poll/poll.service';
import { PollWeekEndDietEditorComponent } from '@modules/ui/components/user/polls/editors/index.polls.editors';
import { AbstractPollPanel } from '@modules/ui/components/user/polls/panels/abstract-poll-panel';

@Component({
  selector: 'app-poll-week-end-diet-panel',
  templateUrl: './poll-week-end-diet-panel.component.html',
  styleUrls: ['./poll-week-end-diet-panel.component.scss']
})
export class PollWeekEndDietPanelComponent extends AbstractPollPanel<PollWeekEndDiet, PollWeekEndDietEditorComponent> {

  constructor(protected editorService: PollService<PollWeekEndDiet>) {
    super(editorService, PollWeekEndDietEditorComponent);
  }
}
