import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollWeekDietPanelComponent } from './poll-week-diet-panel.component';

describe('PollWeekDietPanelComponent', () => {
  let component: PollWeekDietPanelComponent;
  let fixture: ComponentFixture<PollWeekDietPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollWeekDietPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWeekDietPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
