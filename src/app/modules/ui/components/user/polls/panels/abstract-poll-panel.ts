import { ComponentType } from '@angular/cdk/overlay';
import { EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { AbstractPoll } from '@models/polls/index.polls';
import { PollService } from '@modules/global/services/index.services';
import { AbstractPollEditor } from '@modules/ui/components/user/polls/editors/index.polls.editors';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';


export class AbstractPollPanel<P extends AbstractPoll, E extends AbstractPollEditor<P, AbstractForm<P>>> {

    @Input()
    poll: P;

    @Output()
    updatedPoll: EventEmitter<P> = new EventEmitter();

    constructor(
        protected editorService: PollService<P>,
        protected editor: ComponentType<E> | TemplateRef<E>
    ) { }

    openDialog(poll: P): void {
        this.editorService.show(this.editor, poll).subscribe(result => {
            if (result) {
                this.updatedPoll.emit(result);
            }
        });
    }
}
