import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollClinicDataPanelComponent } from './poll-clinic-data-panel.component';

describe('PollClinicDataPanelComponent', () => {
  let component: PollClinicDataPanelComponent;
  let fixture: ComponentFixture<PollClinicDataPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollClinicDataPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollClinicDataPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
