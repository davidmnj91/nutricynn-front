import { Component, Input } from '@angular/core';
import { UserDiet } from '@models/index.models';
import { UserDietService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user-diet-history',
  templateUrl: './user-diet-history.component.html',
  styleUrls: ['./user-diet-history.component.scss']
})
export class UserDietHistoryComponent {

  @Input()
  diet: UserDiet;

  constructor() {
  }

}
