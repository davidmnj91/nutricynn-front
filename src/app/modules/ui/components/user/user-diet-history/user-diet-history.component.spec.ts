import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDietHistoryComponent } from './user-diet-history.component';

describe('UserDietHistoryComponent', () => {
  let component: UserDietHistoryComponent;
  let fixture: ComponentFixture<UserDietHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDietHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDietHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
