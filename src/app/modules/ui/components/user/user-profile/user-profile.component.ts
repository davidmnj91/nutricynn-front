import { Component, Input } from '@angular/core';
import { UserAccount } from '@models/index.models';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent {

  @Input()
  user: UserAccount;

  constructor() { }
}
