import { Component, Input } from '@angular/core';
import { DietMeal, WeekDaysEnum } from '@models/index.models';


@Component({
  selector: 'app-diet-day',
  templateUrl: './diet-day.component.html',
  styleUrls: ['./diet-day.component.scss']
})
export class DietDayComponent {

  @Input('parent')
  parent: String = '';

  @Input('meal')
  meal: DietMeal;

  @Input('day')
  day: WeekDaysEnum;

  constructor() { }

  private getImage(time: string): string {
    const hour: Number = parseInt(time, 10);
    if (hour > 2 && hour <= 10) { return 'breakfast'; }
    if (hour > 10 && hour <= 13) { return 'brunch'; }
    if (hour > 13 && hour <= 16) { return 'lunch'; }
    if (hour > 16 && hour <= 19) { return 'snack'; }
    if (hour > 19 && hour <= 22) { return 'dinner'; }
    if (hour > 22 && hour < 2) { return 'bed'; }
  }

}
