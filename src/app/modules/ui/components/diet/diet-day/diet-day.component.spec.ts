import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietDayComponent } from './diet-day.component';

describe('DietDayComponent', () => {
  let component: DietDayComponent;
  let fixture: ComponentFixture<DietDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
