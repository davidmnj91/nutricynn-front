import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietFoodComponent } from './diet-food.component';

describe('DietFoodComponent', () => {
  let component: DietFoodComponent;
  let fixture: ComponentFixture<DietFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
