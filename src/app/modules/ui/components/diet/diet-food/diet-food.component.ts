import { Component, Input } from '@angular/core';
import { DietFood } from '@models/index.models';

@Component({
  selector: 'app-diet-food',
  templateUrl: './diet-food.component.html',
  styleUrls: ['./diet-food.component.scss']
})
export class DietFoodComponent {

  @Input('food')
  food: DietFood;

  constructor() { }

}
