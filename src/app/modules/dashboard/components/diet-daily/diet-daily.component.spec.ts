import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietDailyComponent } from './diet-daily.component';

describe('DietDailyComponent', () => {
  let component: DietDailyComponent;
  let fixture: ComponentFixture<DietDailyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietDailyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
