import { Component, OnInit } from '@angular/core';
import { SwiperModule, SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
    selector: 'app-diet-daily',
    templateUrl: './diet-daily.component.html',
    styleUrls: ['./diet-daily.component.scss']
})
export class DietDailyComponent implements OnInit {

    SWIPER_CONFIG: SwiperConfigInterface = {
        effect: 'coverflow',
        loop: true,
        centeredSlides: true,
        slidesPerView: 1,
        keyboard: true,
        mousewheel: true,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
            clickable: true
        }
    };

    constructor() { }

    ngOnInit() { }

}
