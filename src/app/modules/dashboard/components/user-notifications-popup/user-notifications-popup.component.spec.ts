import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNotificationsPopupComponent } from './user-notifications-popup.component';

describe('UserNotificationsPopupComponent', () => {
  let component: UserNotificationsPopupComponent;
  let fixture: ComponentFixture<UserNotificationsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNotificationsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNotificationsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
