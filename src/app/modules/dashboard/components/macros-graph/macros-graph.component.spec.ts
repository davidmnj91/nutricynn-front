import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MacrosGraphComponent } from './macros-graph.component';

describe('MacrosGraphComponent', () => {
  let component: MacrosGraphComponent;
  let fixture: ComponentFixture<MacrosGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MacrosGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacrosGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
