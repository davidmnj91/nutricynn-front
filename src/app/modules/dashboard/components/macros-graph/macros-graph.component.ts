import { Component } from '@angular/core';
import { ChartsModule, Color } from 'ng2-charts';

@Component({
  selector: 'app-macros-graph',
  templateUrl: './macros-graph.component.html',
  styleUrls: ['./macros-graph.component.scss']
})
export class MacrosGraphComponent  {

  private options;
  private data;
  private chart;

  constructor() {   }

    // Doughnut
    public doughnutChartLabels:string[] = ['Fat', 'Carbs', 'Proteins'];
    public doughnutChartData:number[] = [80, 450, 250];
    public doughnutChartType:string = 'doughnut';
    legend: boolean = true;
    colors: any[] = [
       "#f56954",
        "#f39c12",
        "#00a65a"
    ];
    datasets: any[] = [
      {
        data: this.doughnutChartData,
        backgroundColor: [
          "#f56954",
          "#f39c12",
          "#00a65a"
        ],
        hoverBackgroundColor: [
          "#f56954",
          "#f39c12",
          "#00a65a"
        ]
      }];
}
