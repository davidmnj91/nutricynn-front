import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietFullComponent } from './diet-full.component';

describe('DietFullComponent', () => {
  let component: DietFullComponent;
  let fixture: ComponentFixture<DietFullComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietFullComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
