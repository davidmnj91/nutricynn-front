import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
    selector: 'app-diet-full',
    templateUrl: './diet-full.component.html',
    styleUrls: ['./diet-full.component.scss']
})
export class DietFullComponent {

    SWIPER_CONFIG: SwiperConfigInterface = {
        loop: true,
        centeredSlides: true,
        slidesPerView: 3,
        keyboard: true,
        mousewheel: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            hideOnClick: false
        }
    };

    constructor() { }

}
