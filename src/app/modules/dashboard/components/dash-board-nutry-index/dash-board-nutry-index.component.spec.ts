import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashBoardNutryIndexComponent } from './dash-board-nutry-index.component';

describe('DashBoardNutryIndexComponent', () => {
  let component: DashBoardNutryIndexComponent;
  let fixture: ComponentFixture<DashBoardNutryIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashBoardNutryIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashBoardNutryIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
