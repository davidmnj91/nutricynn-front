import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarEventListComponent } from './sidebar-event-list.component';

describe('SidebarEventListComponent', () => {
  let component: SidebarEventListComponent;
  let fixture: ComponentFixture<SidebarEventListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarEventListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
