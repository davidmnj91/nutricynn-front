import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashBoardMainComponent, DashBoardNutryIndexComponent } from './components/index.components';

const dashboardRoutes: Routes = [
    {
        path: '', component: DashBoardMainComponent,
        children: [
            { path: '', component: DashBoardNutryIndexComponent, pathMatch: 'full' },
            { path: 'user', loadChildren: 'app/modules/user/user.module#UserModule' }
        ]
    }
];

export const DashBoardRouter: ModuleWithProviders = RouterModule.forChild(dashboardRoutes);
