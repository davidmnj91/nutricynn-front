import { ModuleWithProviders, NgModule } from '@angular/core';
import { GlobalModule } from '@modules/global/global.module';
import { UiModule } from '@modules/ui/ui.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SwiperModule } from 'ngx-swiper-wrapper';

import {
    DashBoardHeaderComponent,
    DashBoardMainComponent,
    DashBoardNutryIndexComponent,
    DashBoardSidebarComponent,
    DietDailyComponent,
    DietFullComponent,
    MacrosGraphComponent,
    ShoppingListComponent,
    SidebarEventListComponent,
    UserNotificationsPopupComponent,
    WeightGraphComponent,
} from './components/index.components';
import { DashBoardRouter } from './router';


@NgModule({
    declarations: [
        DashBoardHeaderComponent,
        DashBoardNutryIndexComponent,
        MacrosGraphComponent,
        WeightGraphComponent,
        DashBoardMainComponent,
        DietFullComponent,
        DashBoardSidebarComponent,
        DietDailyComponent,
        ShoppingListComponent,
        SidebarEventListComponent,
        UserNotificationsPopupComponent
    ],
    imports: [
        GlobalModule,
        UiModule,
        DashBoardRouter,
        SwiperModule,
        ChartsModule
    ]
})
export class DashboardModule {
    static forRoot(): ModuleWithProviders {
        return { ngModule: DashboardModule };
    }
}
