import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainNavigationComponent } from './components/index.components';
import { AuthGuard, AdminGuard } from '@modules/global/guards/index.guards';

const homeRoutes: Routes = [
    { path: '', component: MainNavigationComponent, pathMatch: 'full' }
];

export const HomeRouter: ModuleWithProviders = RouterModule.forChild(homeRoutes);
