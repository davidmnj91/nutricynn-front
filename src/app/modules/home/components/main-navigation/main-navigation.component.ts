import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '@modules/ui/components/index.components';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent {

  constructor(public dialog: MatDialog) { }

  showLogin() {
    const dialogRef = this.dialog.open(LoginComponent);
  }

}
