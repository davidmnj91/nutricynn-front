import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { GlobalModule } from '@modules/global/global.module';
import {
  MainCardsComponent,
  MainNavigationComponent,
  MainPricingComponent,
} from '@modules/home/components/index.components';
import { UiModule } from '@modules/ui/ui.module';

import { HomeRouter } from './router';


@NgModule({
  imports: [
    CommonModule,
    UiModule,
    GlobalModule,
    HomeRouter
  ],
  declarations: [
    MainNavigationComponent, MainCardsComponent, MainPricingComponent
  ]
})
export class HomeModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: HomeModule };
  }
}
