import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export interface AppConfig {
    apiEndpoint: string;
    apiRestPath: string;
    tokenName: string;
    timeOut: number;
    delay: number;
}

export const APP_DI_CONFIG: AppConfig = {
    apiEndpoint: 'http://localhost:8080/',
    apiRestPath: 'nutry/',
    tokenName: 'token',
    timeOut: 2000,
    delay: 500
};
