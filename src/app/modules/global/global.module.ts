import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import {
    AdminArticleCategoriesEditorComponent,
    AdminArticlesEditorComponent,
    AdminDietEditorComponent,
    AdminEventCategoriesEditorComponent,
    AdminEventEditorComponent,
    AdminMealNameEditorComponent,
    AdminNutrientCategoriesEditorComponent,
    AdminNutrientsEditorComponent,
    AdminUnitsEditorComponent,
    AdminUserEditorComponent,
    UserDietEditorComponent,
    UserProfileEditorComponent,
} from '@modules/admin/components/index.components';
import { AdminGuard, AuthGuard } from '@modules/global/guards/index.guards';
import {
    ArticleCategoryService,
    ArticleService,
    AuthenticationService,
    AuthUtilsService,
    DialogService,
    DietService,
    EditorService,
    EventCategoryService,
    EventService,
    MealNameService,
    NutrientCategoryService,
    NutrientService,
    NutrientUnitService,
    PollBasicDataService,
    PollClinicDataService,
    PollService,
    PollWeekDietService,
    PollWeekEndDietService,
    UserAccountService,
    UserDietService,
} from '@modules/global/services/index.services';
import { TokenInterceptor } from '@modules/global/services/interceptors/index.interceptors';
import {
    ConfirmDialogComponent,
    LoginComponent,
    PollBasicDataEditorComponent,
    PollClinicDataEditorComponent,
    PollWeekDietEditorComponent,
    PollWeekEndDietEditorComponent,
    RegisterComponent,
} from '@modules/ui/components/index.components';
import { MessagesModule, SharedModule } from 'primeng/primeng';

import { APP_CONFIG, APP_DI_CONFIG } from './config/app-config';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { EnumKeysPipe } from './pipes/enum-keys.pipe';


@NgModule({
    declarations: [
        EnumKeysPipe, CapitalizePipe
    ],
    imports: [
        CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule, RouterModule,
        SharedModule, MessagesModule,
        MatButtonModule, MatCheckboxModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatGridListModule,
        MatFormFieldModule, MatInputModule, MatDialogModule, MatOptionModule, MatSidenavModule, MatListModule, MatNativeDateModule,
        MatSelectModule, MatExpansionModule, MatRadioModule, MatTableModule, MatDatepickerModule, MatTabsModule, MatStepperModule
    ],
    exports: [
        CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule, RouterModule,
        SharedModule,
        MatButtonModule, MatCheckboxModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatGridListModule,
        MatFormFieldModule, MatInputModule, MatDialogModule, MatOptionModule, MatSidenavModule, MatListModule, MatDatepickerModule,
        MatSelectModule, MatExpansionModule, MatRadioModule, MatTableModule, MatStepperModule,
        MatTabsModule, MatNativeDateModule, EnumKeysPipe, CapitalizePipe
    ],
    entryComponents: [
        LoginComponent, ConfirmDialogComponent, RegisterComponent,
        AdminDietEditorComponent, AdminMealNameEditorComponent, AdminNutrientsEditorComponent, AdminUserEditorComponent,
        AdminNutrientCategoriesEditorComponent, AdminUnitsEditorComponent, AdminArticlesEditorComponent,
        AdminArticleCategoriesEditorComponent, AdminEventEditorComponent, AdminEventCategoriesEditorComponent,
        PollBasicDataEditorComponent, PollClinicDataEditorComponent, PollWeekDietEditorComponent, PollWeekEndDietEditorComponent,
        UserProfileEditorComponent, UserDietEditorComponent
    ]
})

export class GlobalModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: GlobalModule,
            providers: [
                UserAccountService, ArticleCategoryService, ArticleService, DietService, MealNameService, NutrientCategoryService,
                NutrientUnitService, NutrientService, AuthUtilsService, AuthenticationService, EditorService, DialogService, PollService,
                EventCategoryService, EventService, PollBasicDataService, PollClinicDataService, PollWeekDietService,
                PollWeekEndDietService, UserDietService,
                AuthGuard, AdminGuard,
                { provide: APP_CONFIG, useValue: APP_DI_CONFIG },
                { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
            ]
        };
    }
}
