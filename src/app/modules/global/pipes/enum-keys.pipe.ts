import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'enumKeys'
})
export class EnumKeysPipe implements PipeTransform {

    transform(value, args: string[]): any {
        const keys = [];
        value.forEach((v, k) => {
            keys.push({ key: k, value: v });
        });
        return keys;
    }

}
