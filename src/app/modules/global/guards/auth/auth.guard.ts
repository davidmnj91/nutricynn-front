import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { LoginComponent } from '@modules/ui/components/index.components';
import { AuthUtilsService } from '@modules/global/services/index.services';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authUtils: AuthUtilsService, private router: Router, public dialog: MatDialog) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authUtils.getToken() || this.authUtils.isTokenExpired()) {
      const dialogRef = this.dialog.open(LoginComponent);
      dialogRef.componentInstance.redirectUrl = state.url;
      return false;
    }
    return true;
  }
}
