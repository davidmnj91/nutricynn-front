import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '@modules/ui/components/index.components';
import { AuthUtilsService, AuthenticationService } from '@modules/global/services/index.services';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private authUtils: AuthUtilsService, private router: Router, public dialog: MatDialog) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.authUtils.getToken() || this.authUtils.isTokenExpired()) {
            const dialogRef = this.dialog.open(LoginComponent);
            dialogRef.componentInstance.redirectUrl = state.url;
            return false;
        }
        return true;
    }
}
