import { ComponentType } from '@angular/cdk/overlay';
import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AbstractModel } from '@models/index.models';
import { AbstractEditor } from '@modules/admin/components/index.components';
import { AbstractModalService } from '@modules/global/services/modal/abstract-modal.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class EditorService<M extends AbstractModel> extends AbstractModalService {

    constructor(dialog: MatDialog) {
        super(dialog);
    }

    public show(editor: ComponentType<AbstractEditor<M>> | TemplateRef<AbstractEditor<M>>, data: M): Observable<M> {
        return super.show(editor, data);
    }

    public close(data?: M) {
        super.close(data);
    }

    get config(): MatDialogConfig {
        return {
            disableClose: true,
            maxWidth: '85vw',
            minWidth: '45vw',
            maxHeight: '75vh',
            minHeight: '35vh'
        };
    }
}
