import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { TemplateRef } from '@angular/core/src/linker/template_ref';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';

@Injectable()
export abstract class AbstractModalService {

    protected dialogRef: MatDialogRef<any>;

    constructor(protected dialog: MatDialog) { }

    public show(editor: TemplateRef<any> | ComponentType<any>, data: any): Observable<any> {
        const config = this.config;
        if (data) {
            config.data = data;
        }
        this.dialogRef = this.dialog.open(editor, config);
        return this.dialogRef.afterClosed();
    }

    public close(data?: any) {
        this.dialogRef.close(data);
    }

    get config(): MatDialogConfig {
        return this.config;
    }
}
