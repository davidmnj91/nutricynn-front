import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from '@modules/ui/components/index.components';
import { AbstractModalService } from '@modules/global/services/modal/abstract-modal.service';

@Injectable()
export class DialogService extends AbstractModalService {

    constructor(dialog: MatDialog) {
        super(dialog);
    }

    public confirm(title: string, message: string, item?: any): Observable<boolean> {
        this.dialogRef = this.dialog.open(ConfirmDialogComponent);
        this.dialogRef.componentInstance.title = title;
        this.dialogRef.componentInstance.message = message;
        this.dialogRef.componentInstance.item = item;

        return this.dialogRef.afterClosed();
    }

    get config(): MatDialogConfig {
        return {
            disableClose: true
        };
    }

}
