import { ComponentType } from '@angular/cdk/portal';
import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AbstractPoll } from '@models/polls/index.polls';
import { AbstractModalService } from '@modules/global/services/modal/abstract-modal.service';
import { AbstractPollEditor } from '@modules/ui/components/index.components';
import { AbstractForm } from '@modules/ui/components/user/polls/forms/abstract-form';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PollService<P extends AbstractPoll> extends AbstractModalService {

  constructor(dialog: MatDialog) {
    super(dialog);
  }

  public show(
    editor: ComponentType<AbstractPollEditor<P, AbstractForm<P>>> | TemplateRef<AbstractPollEditor<P, AbstractForm<P>>>,
    data: P): Observable<P> {
    return super.show(editor, data);
  }

  public close(data?: P) {
    super.close(data);
  }

  get config(): MatDialogConfig {
    return {
      disableClose: false,
      minWidth: '45vw',
      maxHeight: '75vh',
      maxWidth: '85vw'
    };
  }

}
