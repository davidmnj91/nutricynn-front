import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { AuthUtilsService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authUtils: AuthUtilsService, @Inject(APP_CONFIG) private config: AppConfig | undefined) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const customHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Nutry-Auth-Token', this.authUtils.getToken());

        request = request.clone({
            url: this.buildUrl(request),
            headers: customHeaders
        });

        return next.handle(request);
    }

    private buildUrl(request: HttpRequest<any>): string {
        return this.config.apiEndpoint + request.url;
    }

}
