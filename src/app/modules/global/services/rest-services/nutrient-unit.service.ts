import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { NutrientUnit } from '@models/index.models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AbstractService } from '@modules/global/services/rest-services/abstract.service';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';

@Injectable()
export class NutrientUnitService extends AbstractService<NutrientUnit> {

    constructor(
        protected http: HttpClient,
        @Inject(APP_CONFIG) protected config: AppConfig | undefined
    ) {
        super(http, config);
        this.restPath = 'nutrientUnit';
    }
}
