import { Observable } from 'rxjs/Observable';

export interface NutryHttpInterface {

    getAll();

    get(id: string);

    post(body: any);

    put(body: any);

    delete(id: string);
}
