import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { UserDiet } from '@models/index.models';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { AbstractService } from '@modules/global/services/rest-services/abstract.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UserDietService extends AbstractService<UserDiet> {

    constructor(
        protected http: HttpClient,
        @Inject(APP_CONFIG) protected config: AppConfig | undefined
    ) {
        super(http, config);
        this.restPath = 'userDiet';
    }

    getByUsername(username: string) {
        return this.http.get<UserDiet[]>(this.config.apiRestPath + this.restPath + '/userName/' + username)
        .subscribe(data => {
            this.dataStore = data;
            this._subject.next(this.dataStore);
        });
    }
}
