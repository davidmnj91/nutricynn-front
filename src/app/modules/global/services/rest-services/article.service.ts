import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Article } from '@models/index.models';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { AbstractService } from '@modules/global/services/rest-services/abstract.service';

@Injectable()
export class ArticleService extends AbstractService<Article> {

    constructor(
        protected http: HttpClient,
        @Inject(APP_CONFIG) protected config: AppConfig | undefined
    ) {
        super(http, config);
        this.restPath = 'article';
    }
}
