import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { AuthUtilsService } from '@modules/global/services/authentication/auth-utils.service';
import { AbstractService } from '@modules/global/services/rest-services/abstract.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserAccountService extends AbstractService<UserAccount> {

    constructor(
        protected http: HttpClient,
        @Inject(APP_CONFIG) protected config: AppConfig | undefined,
        private authUtils: AuthUtilsService
    ) {
        super(http, config);
        this.restPath = 'userAccount';
    }

    getByUsername(username: string): Observable<UserAccount> {
        return this.http.get<UserAccount>(this.config.apiRestPath + this.restPath + '/userName/' + username);
    }

    getCurrentUser(): Observable<UserAccount> {
        return this.getByUsername(this.authUtils.getSubject());
    }
}
