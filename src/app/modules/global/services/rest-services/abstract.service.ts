import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractModel } from '@models/index.models';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { NutryHttpInterface } from '@modules/global/services/rest-services/NutryHttpInterface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';


@Injectable()
export abstract class AbstractService<T extends AbstractModel> implements NutryHttpInterface {

    protected restPath: string;

    protected _subject: BehaviorSubject<T[]>;
    protected dataStore: T[];



    constructor(protected http: HttpClient, @Inject(APP_CONFIG) protected config: AppConfig | undefined) {
        this.dataStore = [];
        this._subject = <BehaviorSubject<T[]>>new BehaviorSubject([]);
    }

    get data$(): Observable<T[]> {
        if (this._subject.isEmpty()) {
            this.getAll();
        }
        return this._subject.asObservable();
    }

    getAll() {
        this.http.get<T[]>(this.config.apiRestPath + this.restPath).subscribe(data => {
            this.dataStore = data;
            this._subject.next(this.dataStore);
        });
    }

    get(id: string) {
        this.http.get<T>(this.config.apiRestPath + this.restPath + '/' + id).subscribe(data => {
            let notFound = true;

            this.dataStore.forEach((item, index) => {
                if (item.id === data.id) {
                    this.dataStore[index] = data;
                    notFound = false;
                }
            });

            if (notFound) {
                this.dataStore = [...this.dataStore, data];
            }

            this._subject.next(this.dataStore);
        });
    }

    post(item: T) {
        this.http.post<T>(this.config.apiRestPath + this.restPath, item).subscribe(data => {
            this.dataStore.forEach((t, i) => {
                if (t.id === item.id) { this.dataStore.splice(i, 1); }
            });
            this.dataStore = [...this.dataStore, data];
            this._subject.next(this.dataStore);
        });
    }

    put(item: T) {
        this.http.put<T>(this.config.apiRestPath + this.restPath, item).subscribe(data => {
            this.dataStore.forEach((t, i) => {
                if (t.id === item.id) { this.dataStore[i] = <T>data; }
            });

            this._subject.next(this.dataStore);
        });
    }

    delete(id: string) {
        this.http.delete(this.config.apiRestPath + this.restPath + '/' + id).subscribe(response => {
            let pos: number;
            this.dataStore.forEach((t, i) => {
                if (t.id === id) { pos = i; return; }
            });
            if (pos !== null) {
                this.dataStore = [...this.dataStore.slice(0, pos), ...this.dataStore.slice(pos + 1)];
            }
            this._subject.next(this.dataStore);
        });
    }
}
