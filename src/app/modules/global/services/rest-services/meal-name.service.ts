import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MealName } from '@models/index.models';
import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { AbstractService } from '@modules/global/services/rest-services/abstract.service';


@Injectable()
export class MealNameService extends AbstractService<MealName> {

    constructor(
        protected http: HttpClient,
        @Inject(APP_CONFIG) protected config: AppConfig | undefined
    ) {
        super(http, config);
        this.restPath = 'mealName';
    }
}
