import { Injectable } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { PollWeekEndDiet } from '@models/polls/index.polls';
import { AuthUtilsService } from '@modules/global/services/authentication/auth-utils.service';
import { AbstractPollService } from '@modules/global/services/rest-services/poll-services/abstract.poll.service';
import { UserAccountService } from '@modules/global/services/rest-services/userAccount.service';

@Injectable()
export class PollWeekEndDietService extends AbstractPollService<PollWeekEndDiet> {
    constructor(protected userAccountService: UserAccountService, protected authUtilsService: AuthUtilsService) {
        super(userAccountService, authUtilsService);
    }

    updateUserPoll(poll: PollWeekEndDiet): UserAccount {
        const changedUser = this.user;
        changedUser.userPolls.pollWeekEndDiet = poll;
        return changedUser;
    }
}
