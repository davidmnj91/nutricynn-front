import { Injectable } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { PollWeekDiet } from '@models/polls/index.polls';
import { AuthUtilsService } from '@modules/global/services/authentication/auth-utils.service';
import { AbstractPollService } from '@modules/global/services/rest-services/poll-services/abstract.poll.service';
import { UserAccountService } from '@modules/global/services/rest-services/userAccount.service';

@Injectable()
export class PollWeekDietService extends AbstractPollService<PollWeekDiet> {
    constructor(protected userAccountService: UserAccountService, protected authUtilsService: AuthUtilsService) {
        super(userAccountService, authUtilsService);
    }

    updateUserPoll(poll: PollWeekDiet): UserAccount {
        const changedUser = this.user;
        changedUser.userPolls.pollWeekDiet = poll;
        return changedUser;
    }
}
