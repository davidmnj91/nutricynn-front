import { Injectable } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { AbstractPoll } from '@models/polls/index.polls';
import { AuthUtilsService } from '@modules/global/services/authentication/auth-utils.service';
import { UserAccountService } from '@modules/global/services/rest-services/userAccount.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export abstract class AbstractPollService<P extends AbstractPoll> {

    private _subject: BehaviorSubject<UserAccount>;

    constructor(protected userAccountService: UserAccountService, protected authUtilsService: AuthUtilsService) { }

    post(poll: P) {
        return this.userAccountService.post(this.updateUserPoll(poll));
    }

    protected get user(): UserAccount {
        if (this._subject.isEmpty()) {
            this.getUserAccount();
        }
        let user = this._subject.asObservable().map(u => user = u);
        return user;
    }

    private getUserAccount() {
        let user: UserAccount;
        this.userAccountService.getByUsername(this.authUtilsService.getSubject()).map(u => user = u);
        return user;
    }

    abstract updateUserPoll(poll: P): UserAccount;
}
