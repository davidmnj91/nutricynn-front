import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthUtilsService } from '@modules/global/services/authentication/auth-utils.service';


@Injectable()
export class AuthenticationService {
    private authPath = 'auth';

    constructor(private authUtils: AuthUtilsService, private http: HttpClient) { }

    public login(username: string, password: string): Observable<any> {
        return this.http.post<any>(this.authPath, JSON.stringify({ username: username, password: password }))
            .map(response => this.authUtils.setToken(response));
    }

    public refreshToken(): Observable<any> {
        return this.http.get<any>(this.authPath + '/refresh')
            .map(response => this.authUtils.setToken(response));
    }
}
