import { APP_CONFIG, AppConfig } from '@modules/global/config/app-config';
import { Injectable, Inject } from '@angular/core';

@Injectable()
export class AuthUtilsService {

    public token: string;

    constructor( @Inject(APP_CONFIG) private config: AppConfig | undefined) { }

    public getToken(): string {
        this.token = localStorage.getItem(this.config.tokenName);
        if (this.token == null) {
            return '';
        }
        return this.token;
    }

    public setToken(token: any) {
        if (token != null) {
            localStorage.setItem(this.config.tokenName, token.token);
        }
    }

    public logout(): void {
        this.token = null;
        localStorage.removeItem(this.config.tokenName);
    }

    public getSubject(): string {
        if (!this.token) {
            this.token = this.getToken();
        }
        if (!this.token) {
            return null;
        }

        return this.decodeToken(this.token).sub;
    }

    public isTokenExpired(): boolean {
        if (!this.token) {
            this.token = this.getToken();
        }
        if (!this.token) {
            return true;
        }

        const date = this.getTokenExpirationDate();
        if (date === undefined) { return false; }
        return !(date.valueOf() > new Date().valueOf());
    }

    private getTokenExpirationDate(): Date {
        const decoded = this.decodeToken(this.token);

        if (decoded.exp === undefined) {
            return null;
        }

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    }

    private decodeToken(token: string): any {
        const parts = token.split('.');

        if (parts.length !== 3) {
            throw new Error('JWT must have 3 parts');
        }

        const decoded = atob(parts[1]);
        if (!decoded) {
            throw new Error('Cannot decode the token');
        }

        return JSON.parse(decoded);
    }
}
