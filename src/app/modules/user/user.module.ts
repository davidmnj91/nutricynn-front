import { ModuleWithProviders, NgModule } from '@angular/core';
import { GlobalModule } from '@modules/global/global.module';
import { UiModule } from '@modules/ui/ui.module';
import { CalendarModule } from 'angular-calendar';

import {
  ProfileEditorComponent,
  UserPageHistoryComponent,
  UserPageIndexComponent,
  UserPagePollsComponent,
  UserPageProfileComponent,
  UserPageScheduleComponent,
} from './components/index.components';
import { UserRouter } from './router';


@NgModule({
  declarations: [
    ProfileEditorComponent,
    UserPageIndexComponent,
    UserPagePollsComponent,
    UserPageProfileComponent,
    UserPageScheduleComponent,
    UserPageHistoryComponent
  ],
  imports: [
    UserRouter,
    GlobalModule,
    UiModule,
    CalendarModule.forRoot()
  ],
  exports: [
    ProfileEditorComponent,
    UserPagePollsComponent,
  ]
})
export class UserModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: UserModule };
  }
}
