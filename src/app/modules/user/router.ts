import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserPageIndexComponent } from './components/index.components';

const userRoutes: Routes = [
    {
        path: '', component: UserPageIndexComponent, pathMatch: 'full'
    }
];

export const UserRouter: ModuleWithProviders = RouterModule.forChild(userRoutes);
