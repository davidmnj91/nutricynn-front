import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPagePollsComponent } from './user-page-polls.component';

describe('UserPagePollsComponent', () => {
  let component: UserPagePollsComponent;
  let fixture: ComponentFixture<UserPagePollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPagePollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPagePollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
