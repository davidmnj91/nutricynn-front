import { Component, Input } from '@angular/core';
import { UserPolls } from '@models/polls/index.polls';


@Component({
    selector: 'app-user-page-polls',
    templateUrl: './user-page-polls.component.html',
    styleUrls: ['./user-page-polls.component.scss']
})
export class UserPagePollsComponent {

    @Input()
    userPolls: UserPolls;

    constructor() { }
}
