import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPageScheduleComponent } from './user-page-schedule.component';

describe('UserPageScheduleComponent', () => {
  let component: UserPageScheduleComponent;
  let fixture: ComponentFixture<UserPageScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPageScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPageScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
