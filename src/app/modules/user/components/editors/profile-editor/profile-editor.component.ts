import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserAccount } from '@models/index.models';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.scss']
})
export class ProfileEditorComponent {

  form: FormGroup;

  @Input()
  user: UserAccount;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group({
      'id': [],
      'username': ['', Validators.required],
      'email': ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
      'password': ['', Validators.required],
      'roles': [null, Validators.required],
      'enabled': [true, Validators.required],
      'name': ['', Validators.required],
      'lastName': ['', Validators.required],
      'photoPath': ['', Validators.required],
      'birthday': ['', Validators.required],
      'phone': ['', Validators.required],
      'address': ['', Validators.required],
      'weight': ['', Validators.required],
      'height': ['', Validators.required]
    });
  }

}
