import { Component } from '@angular/core';
import { UserAccount } from '@models/index.models';
import { UserPolls } from '@models/polls/index.polls';
import { UserAccountService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user-page-index',
  templateUrl: './user-page-index.component.html',
  styleUrls: ['./user-page-index.component.scss']
})
export class UserPageIndexComponent {

  user: UserAccount;

  constructor(userAccountService: UserAccountService) {
    userAccountService.getCurrentUser().subscribe((u: UserAccount) => this.user = u);
  }

}
