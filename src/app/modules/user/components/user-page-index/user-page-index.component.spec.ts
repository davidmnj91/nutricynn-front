import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPageIndexComponent } from './user-page-index.component';

describe('UserPageIndexComponent', () => {
  let component: UserPageIndexComponent;
  let fixture: ComponentFixture<UserPageIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPageIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPageIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
