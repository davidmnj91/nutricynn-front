import { Component, Input } from '@angular/core';
import { UserAccount } from '@models/index.models';

@Component({
  selector: 'app-user-page-profile',
  templateUrl: './user-page-profile.component.html',
  styleUrls: ['./user-page-profile.component.scss']
})
export class UserPageProfileComponent {

  @Input()
  user: UserAccount;

  constructor() { }

}
