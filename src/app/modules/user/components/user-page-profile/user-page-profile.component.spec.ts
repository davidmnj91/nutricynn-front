import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPageProfileComponent } from './user-page-profile.component';

describe('UserPageProfileComponent', () => {
  let component: UserPageProfileComponent;
  let fixture: ComponentFixture<UserPageProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPageProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPageProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
