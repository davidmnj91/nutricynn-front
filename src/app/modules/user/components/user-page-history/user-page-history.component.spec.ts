import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPageHistoryComponent } from './user-page-history.component';

describe('UserPageHistoryComponent', () => {
  let component: UserPageHistoryComponent;
  let fixture: ComponentFixture<UserPageHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPageHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPageHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
