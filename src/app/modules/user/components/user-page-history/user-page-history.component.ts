import { Component, Input } from '@angular/core';
import { UserDiet } from '@models/index.models';
import { UserDietService } from '@modules/global/services/index.services';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user-page-history',
  templateUrl: './user-page-history.component.html',
  styleUrls: ['./user-page-history.component.scss']
})
export class UserPageHistoryComponent {

  data$: Observable<UserDiet[]>;

  @Input()
  userName: string;

  constructor(userDietSerivce: UserDietService) {
    this.data$ = userDietSerivce.data$;
    userDietSerivce.getByUsername(this.userName);
  }

}
