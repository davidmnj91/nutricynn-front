import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GlobalModule } from '@modules/global/global.module';

import { AppComponent } from './app.component';
import { AppRouter } from './router';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRouter,
    GlobalModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
